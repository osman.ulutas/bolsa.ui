import { ThemeProvider } from '@mui/material/styles';
import { CssBaseline, StyledEngineProvider } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import React, { useState, useEffect } from 'react';

// routing
import Routes from 'routes';

// defaultTheme
import themes from 'themes';

// project imports
import NavigationScroll from 'layout/NavigationScroll';

import { getRoomType, getChildPolicy, getCategory, getStar, getCancellationPolicy } from 'store/actions/hotelsettings.actions';
import { getCurrency } from 'store/actions/currency.actions';
import { getPaymentType } from 'store/actions/payment.actions';
import { getCountry } from 'store/actions/country.actions';

// ==============================|| APP ||============================== //

const App = () => {
    const dispatch = useDispatch();

    const customization = useSelector((state) => state.customization);

    useEffect(() => {
        dispatch(getRoomType());
        dispatch(getChildPolicy());
        dispatch(getCategory());
        dispatch(getStar());
        dispatch(getCancellationPolicy());
        dispatch(getCurrency());
        dispatch(getPaymentType());
        dispatch(getCountry());
    }, []);

    return (
        <StyledEngineProvider injectFirst>
            <ThemeProvider theme={themes(customization)}>
                <CssBaseline />
                <NavigationScroll>
                    <Routes />
                </NavigationScroll>
            </ThemeProvider>
        </StyledEngineProvider>
    );
};

export default App;
