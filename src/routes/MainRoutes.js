import { lazy } from 'react';

// project imports
import MainLayout from 'layout/MainLayout';
import Loadable from 'ui-component/Loadable';

// dashboard routing
const DashboardDefault = Loadable(lazy(() => import('views/dashboard/Default')));

// utilities routing
const UtilsCurrency = Loadable(lazy(() => import('views/utilities/Currency')));
const UtilsPaymentType = Loadable(lazy(() => import('views/utilities/PaymentType')));

const UtilsHotels = Loadable(lazy(() => import('views/utilities/Hotels')));
const UtilsHotelCreate = Loadable(lazy(() => import('views/utilities/HotelCreate')));
const UtilsHotelEdit = Loadable(lazy(() => import('views/utilities/HotelEdit')));
const UtilsHotelSettings = Loadable(lazy(() => import('views/utilities/HotelSettings')));

const UtilsTours = Loadable(lazy(() => import('views/utilities/Tours')));
const UtilsTourCreate = Loadable(lazy(() => import('views/utilities/TourCreate')));
const UtilsTourEdit = Loadable(lazy(() => import('views/utilities/TourEdit')));
const UtilsTourSettings = Loadable(lazy(() => import('views/utilities/TourSettings')));

const UtilsHotelChildPolicyDetail = Loadable(lazy(() => import('views/utilities/ChildPolicyDetail')));
const UtilsHotelCancellationPolicy = Loadable(lazy(() => import('views/utilities/CancellationPolicyDetail')));

// sample page routing
const SamplePage = Loadable(lazy(() => import('views/sample-page')));

// ==============================|| MAIN ROUTING ||============================== //

const MainRoutes = {
    path: '/',
    element: <MainLayout />,
    children: [
        {
            path: '/',
            element: <DashboardDefault />
        },
        {
            path: '/dashboard/default',
            element: <DashboardDefault />
        },
        {
            path: '/settings/currency',
            element: <UtilsCurrency />
        },
        {
            path: '/settings/paymentType',
            element: <UtilsPaymentType />
        },

        {
            path: '/hotels/list',
            element: <UtilsHotels />
        },
        {
            path: '/hotels/create',
            element: <UtilsHotelCreate />
        },
        {
            path: '/hotels/settings',
            element: <UtilsHotelSettings />
        },

        {
            path: '/hotels/edit/:id',
            element: <UtilsHotelEdit />
        },

        {
            path: '/tours/list',
            element: <UtilsTours />
        },
        {
            path: '/tours/create',
            element: <UtilsTourCreate />
        },
        {
            path: '/tours/settings',
            element: <UtilsTourSettings />
        },

        {
            path: '/tours/edit/:id',
            element: <UtilsTourEdit />
        },

        {
            path: '/hotels/settings/childpolicydetail/:id',
            element: <UtilsHotelChildPolicyDetail />
        },
        {
            path: '/hotels/settings/cancellationpolicydetail/:id',
            element: <UtilsHotelCancellationPolicy />
        },
        {
            path: '/sample-page',
            element: <SamplePage />
        }
    ]
};

export default MainRoutes;
