// assets
import { IconTypography, IconPalette, IconShadow, IconWindmill, IconSettings, IconBuildingSkyscraper, IconRocket } from '@tabler/icons';

// constant
const icons = {
    IconTypography,
    IconPalette,
    IconShadow,
    IconWindmill,
    IconSettings,
    IconBuildingSkyscraper,
    IconRocket
};

// ==============================|| UTILITIES MENU ITEMS ||============================== //

const utilities = {
    id: 'utilities',
    title: 'Utilities',
    type: 'group',
    children: [
        {
            id: 'Hotels',
            title: 'Hotels',
            type: 'collapse',
            icon: icons.IconBuildingSkyscraper,
            children: [
                {
                    id: 'hotel-list',
                    title: 'Hotel List',
                    type: 'item',
                    url: '/hotels/list',
                    breadcrumbs: false
                },
                {
                    id: 'hotel-create',
                    title: 'Hotel Create',
                    type: 'item',
                    url: '/hotels/create',
                    breadcrumbs: false
                },
                {
                    id: 'hotel-settings',
                    title: 'Hotel Settings',
                    type: 'item',
                    url: '/hotels/settings',
                    breadcrumbs: false
                }
            ]
        },

        {
            id: 'Tours',
            title: 'Tours',
            type: 'collapse',
            icon: icons.IconRocket,
            children: [
                {
                    id: 'tour-list',
                    title: 'Tour List',
                    type: 'item',
                    url: '/tours/list',
                    breadcrumbs: false
                },
                {
                    id: 'tour-create',
                    title: 'Tour Create',
                    type: 'item',
                    url: '/tours/create',
                    breadcrumbs: false
                },
                {
                    id: 'tour-settings',
                    title: 'Tour Settings',
                    type: 'item',
                    url: '/tours/settings',
                    breadcrumbs: false
                }
            ]
        },
        {
            id: 'Settings',
            title: 'Settings',
            type: 'collapse',
            icon: icons.IconSettings,
            children: [
                {
                    id: 'currency',
                    title: 'Currency',
                    type: 'item',
                    url: '/settings/currency',
                    breadcrumbs: false
                },
                {
                    id: 'payment',
                    title: 'Payment Type',
                    type: 'item',
                    url: '/settings/paymentType',
                    breadcrumbs: false
                }
            ]
        }
    ]
};

export default utilities;
