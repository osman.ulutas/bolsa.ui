import { tourSettingsConstants } from 'store/constants';
import { client } from '_helpers/axios-settings';

export const addTravelLocation = (obj) => {
    debugger;
    return async (dispatch) => {
        dispatch({ type: tourSettingsConstants.ADD_TS_TRAVELLOCATION_REQUEST });
        try {
            const res = await client.post('bolsa/TravelLocation/travellocation', { ...obj });

            if (res.status === 200) {
                const newRow = {
                    id: res.data.data.id,
                    col1: res.data.data.name,
                    col2: res.data.data.country.name,
                    col3: res.data.data.city.name,
                    col4: res.data.data.distriction.name,
                    col5: res.data.data.countryId,
                    col6: res.data.data.cityId,
                    col7: res.data.data.districtionId
                };

                dispatch({ type: tourSettingsConstants.ADD_TS_TRAVELLOCATION_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: tourSettingsConstants.ADD_TS_TRAVELLOCATION_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: tourSettingsConstants.ADD_TS_TRAVELLOCATION_ERROR, payload: error });
        }
    };
};

export const getTravelLocation = () => {
    return async (dispatch) => {
        dispatch({ type: tourSettingsConstants.GET_TS_TRAVELLOCATION_REQUEST });
        try {
            const res = await client.get('bolsa/TravelLocation/travellocation');

            if (res.status === 200) {
                dispatch({ type: tourSettingsConstants.GET_TS_TRAVELLOCATION_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: tourSettingsConstants.GET_TS_TRAVELLOCATION_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: tourSettingsConstants.GET_TS_TRAVELLOCATION_ERROR, payload: error });
        }
    };
};

export const deleteTravelLocation = (id) => {
    return async (dispatch) => {
        dispatch({ type: tourSettingsConstants.REMOVE_TS_TRAVELLOCATION_REQUEST });
        try {
            const res = await client.delete(`bolsa/TravelLocation/travellocation/${id}`);

            if (res.status === 204) {
                dispatch({ type: tourSettingsConstants.REMOVE_TS_TRAVELLOCATION_SUCCESS, payload: id });
            } else {
                dispatch({ type: tourSettingsConstants.REMOVE_TS_TRAVELLOCATION_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: tourSettingsConstants.REMOVE_TS_TRAVELLOCATION_ERROR, payload: error });
        }
    };
};

export const updateTravelLocation = (obj) => {
    debugger;
    return async (dispatch) => {
        dispatch({ type: tourSettingsConstants.UPDATE_TS_TRAVELLOCATION_REQUEST });
        try {
            const res = await client.put(`bolsa/TravelLocation/travellocation/${obj.id}`, {
                name: obj.col1,
                countryId: obj.col5,
                cityId: obj.col6,
                districtionId: obj.col7
            });

            if (res.status === 204) {
                dispatch({ type: tourSettingsConstants.UPDATE_TS_TRAVELLOCATION_SUCCESS, payload: obj });
            } else {
                dispatch({ type: tourSettingsConstants.UPDATE_TS_TRAVELLOCATION_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: tourSettingsConstants.UPDATE_TS_TRAVELLOCATION_ERROR, payload: error });
        }
    };
};

export const addTravelLocationItem = (obj) => {
    debugger;
    return async (dispatch) => {
        dispatch({ type: tourSettingsConstants.ADD_TS_TRAVELLOCATIONITEM_REQUEST });
        try {
            const res = await client.post('bolsa/TravelLocation/travellocationitem', { ...obj });

            if (res.status === 200) {
                const newRow = {
                    id: res.data.data.id,
                    col1: res.data.data.name,
                    col2: res.data.data.country.name,
                    col3: res.data.data.city.name,
                    col4: res.data.data.distriction.name,
                    col5: res.data.data.countryId,
                    col6: res.data.data.cityId,
                    col7: res.data.data.districtionId,
                    col8: res.data.data.canSalePart,
                    col9: res.data.data.isOther,
                    col10: res.data.data.paymentTypeId,
                    col11: res.data.data.paymentType.name,
                    col12: res.data.data.optionalStatus === 0 ? 'Default' : 'Optinal',
                    col13: res.data.data.optionalStatus
                };

                dispatch({ type: tourSettingsConstants.ADD_TS_TRAVELLOCATIONITEM_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: tourSettingsConstants.ADD_TS_TRAVELLOCATIONITEM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: tourSettingsConstants.ADD_TS_TRAVELLOCATIONITEM_ERROR, payload: error });
        }
    };
};

export const getTravelLocationItem = () => {
    return async (dispatch) => {
        dispatch({ type: tourSettingsConstants.GET_TS_TRAVELLOCATIONITEM_REQUEST });
        try {
            const res = await client.get('bolsa/TravelLocation/travellocationitem');

            if (res.status === 200) {
                dispatch({ type: tourSettingsConstants.GET_TS_TRAVELLOCATIONITEM_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: tourSettingsConstants.GET_TS_TRAVELLOCATIONITEM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: tourSettingsConstants.GET_TS_TRAVELLOCATIONITEM_ERROR, payload: error });
        }
    };
};

export const deleteTravelLocationItem = (id) => {
    return async (dispatch) => {
        dispatch({ type: tourSettingsConstants.REMOVE_TS_TRAVELLOCATIONITEM_REQUEST });
        try {
            const res = await client.delete(`bolsa/TravelLocation/travellocationitem/${id}`);

            if (res.status === 204) {
                dispatch({ type: tourSettingsConstants.REMOVE_TS_TRAVELLOCATIONITEM_SUCCESS, payload: id });
            } else {
                dispatch({ type: tourSettingsConstants.REMOVE_TS_TRAVELLOCATIONITEM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: tourSettingsConstants.REMOVE_TS_TRAVELLOCATIONITEM_ERROR, payload: error });
        }
    };
};

export const updateTravelLocationItem = (obj) => {
    debugger;
    return async (dispatch) => {
        dispatch({ type: tourSettingsConstants.UPDATE_TS_TRAVELLOCATIONITEM_REQUEST });
        try {
            const res = await client.put(`bolsa/TravelLocation/travellocationitem/${obj.id}`, {
                ...obj
            });

            if (res.status !== 204) {
                dispatch({ type: tourSettingsConstants.UPDATE_TS_TRAVELLOCATIONITEM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: tourSettingsConstants.UPDATE_TS_TRAVELLOCATIONITEM_ERROR, payload: error });
        }
    };
};

export const addTravelLocationCurrency = (obj) => {
    debugger;
    return async (dispatch) => {
        dispatch({ type: tourSettingsConstants.ADD_TS_CURRENCY_REQUEST });
        try {
            const res = await client.post('bolsa/TravelLocation/travellocationitemprice', { ...obj });

            if (res.status === 200) {
                const newRow = {
                    id: res.data.data.id,
                    col1: res.data.data.currency.name,
                    col2: res.data.data.cost,
                    col3: res.data.data.price
                };

                dispatch({ type: tourSettingsConstants.ADD_TS_CURRENCY_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: tourSettingsConstants.ADD_TS_CURRENCY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: tourSettingsConstants.ADD_TS_CURRENCY_ERROR, payload: error });
        }
    };
};

export const getTravelLocationItemCurrency = (id) => {
    return async (dispatch) => {
        debugger;
        dispatch({ type: tourSettingsConstants.GET_TS_CURRENCY_REQUEST });
        try {
            const res = await client.get(`bolsa/TravelLocation/travellocationpricebyItemId/${id}`);

            if (res.status === 200) {
                dispatch({ type: tourSettingsConstants.GET_TS_CURRENCY_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: tourSettingsConstants.GET_TS_CURRENCY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: tourSettingsConstants.GET_TS_CURRENCY_ERROR, payload: error });
        }
    };
};

export const deleteTravelLocationItemCurrency = (id) => {
    return async (dispatch) => {
        dispatch({ type: tourSettingsConstants.REMOVE_TS_CURRENCY_REQUEST });
        try {
            const res = await client.delete(`bolsa/TravelLocation/travellocationitemprice/${id}`);

            if (res.status === 204) {
                dispatch({ type: tourSettingsConstants.REMOVE_TS_CURRENCY_SUCCESS, payload: id });
            } else {
                dispatch({ type: tourSettingsConstants.REMOVE_TS_CURRENCY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: tourSettingsConstants.REMOVE_TS_CURRENCY_ERROR, payload: error });
        }
    };
};

export const updateTravelLocationItemCurrency = (obj) => {
    debugger;
    return async (dispatch) => {
        dispatch({ type: tourSettingsConstants.UPDATE_TS_CURRENCY_REQUEST });
        try {
            const res = await client.put(`bolsa/TravelLocation/travellocationitemprice/${obj.id}`, {
                id: Number(obj.id),
                currencyId: Number(obj.currencyId),
                travelLocationItemId: Number(obj.travelLocationItemId),
                cost: Number(obj.col2),
                price: Number(obj.col3)
            });

            if (res.status === 204) {
                dispatch({ type: tourSettingsConstants.UPDATE_TS_CURRENCY_SUCCESS, payload: obj });
            } else {
                dispatch({ type: tourSettingsConstants.UPDATE_TS_CURRENCY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: tourSettingsConstants.UPDATE_TS_CURRENCY_ERROR, payload: error });
        }
    };
};
