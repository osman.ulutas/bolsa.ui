import { paymentTypeConstants } from 'store/constants/';
import { client } from '_helpers/axios-settings';

export const addPaymentType = (name) => {
    return async (dispatch) => {
        dispatch({ type: paymentTypeConstants.ADD_PAYMENT_TYPE_REQUEST });
        try {
            const res = await client.post('bolsa/Payment/paymenttype', { name });

            if (res.status === 200) {
                const newRow = { id: res.data.data.id, col1: res.data.data.name };

                dispatch({ type: paymentTypeConstants.ADD_PAYMENT_TYPE_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: paymentTypeConstants.ADD_PAYMENT_TYPE_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: paymentTypeConstants.ADD_PAYMENT_TYPE_ERROR, payload: error });
        }
    };
};

export const getPaymentType = () => {
    return async (dispatch) => {
        dispatch({ type: paymentTypeConstants.GET_PAYMENT_TYPE_REQUEST });
        try {
            const res = await client.get('bolsa/Payment/paymenttype');

            if (res.status === 200) {
                dispatch({ type: paymentTypeConstants.GET_PAYMENT_TYPE_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: paymentTypeConstants.GET_PAYMENT_TYPE_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: paymentTypeConstants.GET_PAYMENT_TYPE_ERROR, payload: error });
        }
    };
};

export const deletePaymentType = (id) => {
    return async (dispatch) => {
        dispatch({ type: paymentTypeConstants.REMOVE_PAYMENT_TYPE_REQUEST });
        try {
            const res = await client.delete(`bolsa/Payment/paymenttype/${id}`);

            if (res.status === 204) {
                dispatch({ type: paymentTypeConstants.REMOVE_PAYMENT_TYPE_SUCCESS, payload: id });
            } else {
                dispatch({ type: paymentTypeConstants.REMOVE_PAYMENT_TYPE_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: paymentTypeConstants.REMOVE_PAYMENT_TYPE_ERROR, payload: error });
        }
    };
};

export const updatePaymentType = (obj) => {
    return async (dispatch) => {
        dispatch({ type: paymentTypeConstants.UPDATE_PAYMENT_TYPE_REQUEST });
        try {
            const res = await client.put(`bolsa/Payment/paymenttype/${obj.id}`, { name: obj.col1 });

            if (res.status === 204) {
                dispatch({ type: paymentTypeConstants.UPDATE_PAYMENT_TYPE_SUCCESS, payload: obj });
            } else {
                dispatch({ type: paymentTypeConstants.UPDATE_PAYMENT_TYPE_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: paymentTypeConstants.UPDATE_PAYMENT_TYPE_ERROR, payload: error });
        }
    };
};
