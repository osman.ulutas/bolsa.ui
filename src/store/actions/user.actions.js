import { userConstants } from 'store/constants';
import { authHeader } from '_helpers/auth-header';
import { client } from '_helpers/axios-settings';
import { history } from '_helpers/history';

export const login = (email, password) => {
    return async (dispatch) => {
        dispatch({ type: userConstants.LOGIN_REQUEST, email });
        try {
            const user = await client.get('bolsa/Currency/currencies');
            console.log(user);

            localStorage.setItem('user', JSON.stringify(user));
            dispatch({ type: userConstants.LOGIN_SUCCESS, user });
        } catch (error) {
            dispatch({ type: userConstants.LOGIN_ERROR, error });
        }
    };
};
