import { currencyConstants } from 'store/constants';
import { client } from '_helpers/axios-settings';

export const addCurrency = (name, symbol) => {
    return async (dispatch) => {
        dispatch({ type: currencyConstants.ADD_CURRENCY_REQUEST });
        try {
            const res = await client.post('bolsa/Currency/currencies', { name, symbol });

            if (res.status === 200) {
                const newRow = { id: res.data.data.id, col1: res.data.data.name, col2: res.data.data.symbol };

                dispatch({ type: currencyConstants.ADD_CURRENCY_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: currencyConstants.ADD_CURRENCY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: currencyConstants.ADD_CURRENCY_ERROR, payload: error });
        }
    };
};

export const getCurrency = () => {
    return async (dispatch) => {
        dispatch({ type: currencyConstants.GET_CURRENCY_REQUEST });
        try {
            const res = await client.get('bolsa/Currency/currencies');

            if (res.status === 200) {
                dispatch({ type: currencyConstants.GET_CURRENCY_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: currencyConstants.GET_CURRENCY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: currencyConstants.GET_CURRENCY_ERROR, payload: error });
        }
    };
};

export const deleteCurrency = (id) => {
    return async (dispatch) => {
        dispatch({ type: currencyConstants.REMOVE_CURRENCY_REQUEST });
        try {
            const res = await client.delete(`bolsa/Currency/currencies/${id}`);

            if (res.status === 204) {
                dispatch({ type: currencyConstants.REMOVE_CURRENCY_SUCCESS, payload: id });
            } else {
                dispatch({ type: currencyConstants.REMOVE_CURRENCY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: currencyConstants.REMOVE_CURRENCY_ERROR, payload: error });
        }
    };
};

export const updateCurrency = (obj) => {
    return async (dispatch) => {
        dispatch({ type: currencyConstants.UPDATE_CURRENCY_REQUEST });
        try {
            const res = await client.put(`bolsa/Currency/currencies/${obj.id}`, { name: obj.col1, symbol: obj.col2 });

            if (res.status === 204) {
                dispatch({ type: currencyConstants.UPDATE_CURRENCY_SUCCESS, payload: obj });
            } else {
                dispatch({ type: currencyConstants.UPDATE_CURRENCY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: currencyConstants.UPDATE_CURRENCY_ERROR, payload: error });
        }
    };
};
