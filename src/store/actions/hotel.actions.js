import { hotelConstants } from 'store/constants';
import { client } from '_helpers/axios-settings';

export const addHotel = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.ADD_HOTEL_REQUEST });
        try {
            const res = await client.post('bolsa/Hotel/hotels', { ...obj });

            if (res.status === 200) {
                const newRow = { id: res.data.data.id, col1: res.data.data.stars };

                dispatch({ type: hotelConstants.ADD_HOTEL_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: hotelConstants.ADD_HOTEL_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.ADD_HOTEL_ERROR, payload: error });
        }
    };
};

export const getHotel = () => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.GET_HOTEL_REQUEST });
        try {
            const res = await client.get('bolsa/Hotel/hotels');

            if (res.status === 200) {
                dispatch({ type: hotelConstants.GET_HOTEL_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: hotelConstants.GET_HOTEL_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.GET_HOTEL_ERROR, payload: error });
        }
    };
};

export const deleteHotel = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.REMOVE_HOTEL_REQUEST });
        try {
            const res = await client.delete(`bolsa/Hotel/hotels/${id}`);

            if (res.status === 204) {
                dispatch({ type: hotelConstants.REMOVE_HOTEL_SUCCESS, payload: id });
            } else {
                dispatch({ type: hotelConstants.REMOVE_HOTEL_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.REMOVE_HOTEL_ERROR, payload: error });
        }
    };
};

export const updateHotel = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.UPDATE_HOTEL_REQUEST });
        try {
            const res = await client.put(`bolsa/Hotel/hotels/${obj.id}`, { ...obj });

            if (res.status === 204) {
                dispatch({ type: hotelConstants.UPDATE_HOTEL_SUCCESS, payload: obj });
            } else {
                dispatch({ type: hotelConstants.UPDATE_HOTEL_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.UPDATE_HOTEL_ERROR, payload: error });
        }
    };
};

export const getOneHotel = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.GET_ONE_HOTEL_REQUEST });
        try {
            const res = await client.get(`bolsa/Hotel/hotels/${id}`);

            if (res.status === 200) {
                dispatch({ type: hotelConstants.GET_ONE_HOTEL_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: hotelConstants.GET_ONE_HOTEL_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.GET_ONE_HOTEL_ERROR, payload: error });
        }
    };
};

export const addHotelRoom = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.ADD_HOTEL_ROOM_REQUEST });
        try {
            const res = await client.post('/bolsa/Hotel/AddRoomtoHotel', { ...obj });

            if (res.status === 200) {
                const newRow = {
                    id: res.data.data.id,
                    col1: res.data.data.roomType.name,
                    col2: res.data.data.roomType.child,
                    col3: res.data.data.roomType.adult
                };

                dispatch({ type: hotelConstants.ADD_HOTEL_ROOM_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: hotelConstants.ADD_HOTEL_ROOM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.ADD_HOTEL_ROOM_ERROR, payload: error });
        }
    };
};

export const addHotelChild = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.ADD_HOTEL_CILD_REQUEST });
        try {
            const res = await client.post('/bolsa/Hotel/AddChildPolicy', { ...obj });

            if (res.status === 200) {
                const newRow = { id: res.data.data.id, col1: res.data.data.childPolicy.name };

                dispatch({ type: hotelConstants.ADD_HOTEL_CHILD_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: hotelConstants.ADD_HOTEL_CHILD_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.ADD_HOTEL_CHILD_ERROR, payload: error });
        }
    };
};

export const addHotelCancellation = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.ADD_HOTEL_CANCELLATION_REQUEST });
        try {
            const res = await client.post('/bolsa/Hotel/AddCancellationPolicyById', { ...obj });

            if (res.status === 200) {
                const newRow = { id: res.data.data.id, col1: res.data.data.cancellationPolicy.name };

                dispatch({ type: hotelConstants.ADD_HOTEL_CANCELLATION_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: hotelConstants.ADD_HOTEL_CANCELLATION_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.ADD_HOTEL_CANCELLATION_ERROR, payload: error });
        }
    };
};

export const addHotelContract = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.ADD_HOTEL_CONTRACT_REQUEST });
        try {
            const res = await client.post('/bolsa/Hotel/hotelcontract', { ...obj });

            if (res.status === 200) {
                let obj = { id: '', col1: '', col2: '', col3: '', col4: '', col5: '', col6: '', col7: '', col8: '' };
                obj = {
                    id: res.data.data.id,
                    col1: res.data.data.startDate,
                    col2: res.data.data.endDate
                };
                const arrObj = res.data.data.hotelRoomPrice.map((el, index) => {
                    if (el.name === 'Single') {
                        obj.col3 = el.price;
                        obj.col6 = el.id;
                    } else if (el.name === 'Double') {
                        obj.col4 = el.price;
                        obj.col7 = el.id;
                    } else if (el.name === 'Triple') {
                        obj.col5 = el.price;
                        obj.col8 = el.id;
                    }
                    return obj;
                });

                dispatch({ type: hotelConstants.ADD_HOTEL_CONTRACT_SUCCESS, payload: arrObj[arrObj.length - 1] });
            } else {
                dispatch({ type: hotelConstants.ADD_HOTEL_CONTRACT_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.ADD_HOTEL_CONTRACT_ERROR, payload: error });
        }
    };
};

export const deleteHotelRoom = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.REMOVE_HOTEL_ROOM_REQUEST });
        try {
            const res = await client.delete(`bolsa/Hotel/DeleteRoomToHotel/${id}`);

            if (res.status === 204) {
                dispatch({ type: hotelConstants.REMOVE_HOTEL_ROOM_SUCCESS, payload: id });
            } else {
                dispatch({ type: hotelConstants.REMOVE_HOTEL_ROOM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.REMOVE_HOTEL_ROOM_ERROR, payload: error });
        }
    };
};

export const deleteHotelChild = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.REMOVE_HOTEL_CILD_REQUEST });
        try {
            const res = await client.delete(`bolsa/Hotel/DeleteChildPolicyfromHotel/${id}`);

            if (res.status === 200) {
                dispatch({ type: hotelConstants.REMOVE_HOTEL_CHILD_SUCCESS, payload: id });
            } else {
                dispatch({ type: hotelConstants.REMOVE_HOTEL_CHILD_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.REMOVE_HOTEL_CHILD_ERROR, payload: error });
        }
    };
};

export const deleteHotelCancellation = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.REMOVE_HOTEL_CANCELLATION_REQUEST });
        try {
            const res = await client.delete(`bolsa/Hotel/DeleteCancellationPolicyfromHotel/${id}`);

            if (res.status === 200) {
                dispatch({ type: hotelConstants.REMOVE_HOTEL_CANCELLATION_SUCCESS, payload: id });
            } else {
                dispatch({ type: hotelConstants.REMOVE_HOTEL_CANCELLATION_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.REMOVE_HOTEL_CANCELLATION_ERROR, payload: error });
        }
    };
};

export const deleteHotelContract = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.REMOVE_HOTEL_CONTRACT_REQUEST });
        try {
            const res = await client.delete(`bolsa/Hotel/hotelcontract/${id}`);

            if (res.status === 204) {
                dispatch({ type: hotelConstants.REMOVE_HOTEL_CONTRACT_SUCCESS, payload: id });
            } else {
                dispatch({ type: hotelConstants.REMOVE_HOTEL_CONTRACT_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.REMOVE_HOTEL_CONTRACT_ERROR, payload: error });
        }
    };
};

export const updateHotelContract = (obj, actId, val) => {
    return async (dispatch) => {
        dispatch({ type: hotelConstants.UPDATE_HOTEL_CONTRACT_REQUEST });
        try {
            const res = await client.put('bolsa/Hotel/hotelcontract/', [{ id: actId, price: val }]);

            if (res.status === 204) {
                dispatch({ type: hotelConstants.UPDATE_HOTEL_CONTRACT_SUCCESS, payload: obj });
            } else {
                dispatch({ type: hotelConstants.UPDATE_HOTEL_CONTRACT_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelConstants.UPDATE_HOTEL_CONTRACT_ERROR, payload: error });
        }
    };
};
