import { hotelSettingsConstants } from 'store/constants';
import { client } from '_helpers/axios-settings';

export const addStar = (stars) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.ADD_STAR_REQUEST });
        try {
            const res = await client.post('bolsa/Hotel/star', { stars });

            if (res.status === 200) {
                const newRow = { id: res.data.data.id, col1: res.data.data.stars };

                dispatch({ type: hotelSettingsConstants.ADD_STAR_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: hotelSettingsConstants.ADD_STAR_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.ADD_STAR_ERROR, payload: error });
        }
    };
};

export const getStar = () => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.GET_STAR_REQUEST });
        try {
            const res = await client.get('bolsa/Hotel/Star');

            if (res.status === 200) {
                dispatch({ type: hotelSettingsConstants.GET_STAR_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: hotelSettingsConstants.GET_STAR_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.GET_STAR_ERROR, payload: error });
        }
    };
};

export const deleteStar = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.REMOVE_STAR_REQUEST });
        try {
            const res = await client.delete(`bolsa/Hotel/star/${id}`);

            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.REMOVE_STAR_SUCCESS, payload: id });
            } else {
                dispatch({ type: hotelSettingsConstants.REMOVE_STAR_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.REMOVE_STAR_ERROR, payload: error });
        }
    };
};

export const updateStar = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.UPDATE_STAR_REQUEST });
        try {
            const res = await client.put(`bolsa/Hotel/star/${obj.id}`, { stars: obj.col1 });

            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.UPDATE_STAR_SUCCESS, payload: obj });
            } else {
                dispatch({ type: hotelSettingsConstants.UPDATE_STAR_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.UPDATE_STAR_ERROR, payload: error });
        }
    };
};

export const addCategory = (name) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.ADD_CATEGORY_REQUEST });
        try {
            const res = await client.post('bolsa/Hotel/hotelcategories', { name });

            if (res.status === 200) {
                const newRow = { id: res.data.data.id, col1: res.data.data.name };

                dispatch({ type: hotelSettingsConstants.ADD_CATEGORY_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: hotelSettingsConstants.ADD_CATEGORY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.ADD_CATEGORY_ERROR, payload: error });
        }
    };
};

export const getCategory = () => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.GET_CATEGORY_REQUEST });
        try {
            const res = await client.get('bolsa/Hotel/hotelcategories');

            if (res.status === 200) {
                dispatch({ type: hotelSettingsConstants.GET_CATEGORY_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: hotelSettingsConstants.GET_CATEGORY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.GET_CATEGORY_ERROR, payload: error });
        }
    };
};

export const deleteCategory = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.REMOVE_CATEGORY_REQUEST });
        try {
            const res = await client.delete(`bolsa/Hotel/hotelcategories/${id}`);

            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.REMOVE_CATEGORY_SUCCESS, payload: id });
            } else {
                dispatch({ type: hotelSettingsConstants.REMOVE_CATEGORY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.REMOVE_CATEGORY_ERROR, payload: error });
        }
    };
};

export const updateCategory = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.UPDATE_CATEGORY_REQUEST });
        try {
            const res = await client.put(`bolsa/Hotel/hotelcategories/${obj.id}`, { name: obj.col1 });

            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.UPDATE_CATEGORY_SUCCESS, payload: obj });
            } else {
                dispatch({ type: hotelSettingsConstants.UPDATE_CATEGORY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.UPDATE_CATEGORY_ERROR, payload: error });
        }
    };
};

export const addRoomType = (name, child, adult) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.ADD_ROOM_TYPE_REQUEST });
        try {
            const res = await client.post('bolsa/Hotel/roomtype', { name, child, adult });

            if (res.status === 200) {
                const newRow = { id: res.data.data.id, col1: res.data.data.name, col2: res.data.data.child, col3: res.data.data.adult };

                dispatch({ type: hotelSettingsConstants.ADD_ROOM_TYPE_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: hotelSettingsConstants.ADD_ROOM_TYPE_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.ADD_ROOM_TYPE_ERROR, payload: error });
        }
    };
};

export const getRoomType = () => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.GET_ROOM_TYPE_REQUEST });
        try {
            const res = await client.get('bolsa/Hotel/roomtype');

            if (res.status === 200) {
                dispatch({ type: hotelSettingsConstants.GET_ROOM_TYPE_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: hotelSettingsConstants.GET_ROOM_TYPE_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.GET_ROOM_TYPE_ERROR, payload: error });
        }
    };
};

export const deleteRoomType = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.REMOVE_ROOM_TYPE_REQUEST });
        try {
            const res = await client.delete(`bolsa/Hotel/roomtype/${id}`);

            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.REMOVE_ROOM_TYPE_SUCCESS, payload: id });
            } else {
                dispatch({ type: hotelSettingsConstants.REMOVE_ROOM_TYPE_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.REMOVE_ROOM_TYPE_ERROR, payload: error });
        }
    };
};

export const updateRoomType = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.UPDATE_ROOM_TYPE_REQUEST });
        try {
            const res = await client.put(`bolsa/Hotel/roomtype/${obj.id}`, { name: obj.col1, child: obj.col2, adult: obj.col3 });

            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.UPDATE_ROOM_TYPE_SUCCESS, payload: obj });
            } else {
                dispatch({ type: hotelSettingsConstants.UPDATE_ROOM_TYPE_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.UPDATE_ROOM_TYPE_ERROR, payload: error });
        }
    };
};

export const addChildPolicyItem = (name, minAge, maxAge, discount, childPolicyId) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.ADD_CHILD_POLICY_ITEM_REQUEST });
        try {
            const res = await client.post('bolsa/Hotel/ChildPolicyItem', { name, minAge, maxAge, discount, childPolicyId });

            if (res.status === 200) {
                const newRow = {
                    id: res.data.data.id,
                    col1: res.data.data.name,
                    col2: res.data.data.minAge,
                    col3: res.data.data.maxAge,
                    col4: res.data.data.discount
                };

                dispatch({ type: hotelSettingsConstants.ADD_CHILD_POLICY_ITEM_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: hotelSettingsConstants.ADD_CHILD_POLICY_ITEM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.ADD_CHILD_POLICY_ITEM_ERROR, payload: error });
        }
    };
};

export const getChildPolicyItem = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.GET_CHILD_POLICY_ITEM_REQUEST });
        try {
            const res = await client.get(`bolsa/Hotel/childpolicy/${id}`);

            if (res.status === 200) {
                dispatch({ type: hotelSettingsConstants.GET_CHILD_POLICY_ITEM_SUCCESS, payload: res.data.data.child });
            } else {
                dispatch({ type: hotelSettingsConstants.GET_CHILD_POLICY_ITEM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.GET_CHILD_POLICY_ITEM_ERROR, payload: error });
        }
    };
};

export const deleteChildPolicyItem = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.REMOVE_CHILD_POLICY_ITEM_REQUEST });
        try {
            const res = await client.delete(`bolsa/Hotel/ChildPolicyItem/${id}`);

            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.REMOVE_CHILD_POLICY_ITEM_SUCCESS, payload: id });
            } else {
                dispatch({ type: hotelSettingsConstants.REMOVE_CHILD_POLICY_ITEM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.REMOVE_CHILD_POLICY_ITEM_ERROR, payload: error });
        }
    };
};

export const updateChildPolicyItem = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.UPDATE_CHILD_POLICY_ITEM_REQUEST });
        try {
            const res = await client.put(`bolsa/Hotel/ChildPolicyItem/${obj.id}`, {
                name: obj.col1,
                minAge: obj.col2,
                maxAge: obj.col3,
                discount: obj.col4
            });

            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.UPDATE_CHILD_POLICY_ITEM_SUCCESS, payload: obj });
            } else {
                dispatch({ type: hotelSettingsConstants.UPDATE_CHILD_POLICY_ITEM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.UPDATE_CHILD_POLICY_ITEM_ERROR, payload: error });
        }
    };
};

export const addChildPolicy = (name) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.ADD_CHILD_POLICY_REQUEST });
        try {
            const res = await client.post('bolsa/Hotel/childpolicy', { name });

            if (res.status === 200) {
                const newRow = { id: res.data.data.id, col1: res.data.data.name };

                dispatch({ type: hotelSettingsConstants.ADD_CHILD_POLICY_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: hotelSettingsConstants.ADD_CHILD_POLICY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.ADD_CHILD_POLICY_ERROR, payload: error });
        }
    };
};

export const getChildPolicy = () => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.GET_CHILD_POLICY_REQUEST });
        try {
            const res = await client.get('bolsa/Hotel/childpolicy');

            if (res.status === 200) {
                dispatch({ type: hotelSettingsConstants.GET_CHILD_POLICY_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: hotelSettingsConstants.GET_CHILD_POLICY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.GET_CHILD_POLICY_ERROR, payload: error });
        }
    };
};

export const deleteChildPolicy = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.REMOVE_CHILD_POLICY_REQUEST });
        try {
            const res = await client.delete(`bolsa/Hotel/childpolicy/${id}`);

            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.REMOVE_CHILD_POLICY_SUCCESS, payload: id });
            } else {
                dispatch({ type: hotelSettingsConstants.REMOVE_CHILD_POLICY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.REMOVE_CHILD_POLICY_ERROR, payload: error });
        }
    };
};

export const updateChildPolicy = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.UPDATE_CHILD_POLICY_REQUEST });
        try {
            const res = await client.put(`bolsa/Hotel/childpolicy/${obj.id}`, {
                name: obj.col1
            });
            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.UPDATE_CHILD_POLICY_SUCCESS, payload: obj });
            } else {
                dispatch({ type: hotelSettingsConstants.UPDATE_CHILD_POLICY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.UPDATE_CHILD_POLICY_ERROR, payload: error });
        }
    };
};

export const addCancellationPolicy = (name) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.ADD_CANCELLATION_POLICY_REQUEST });
        try {
            const res = await client.post('bolsa/Hotel/cancellationpolicy', { name });

            if (res.status === 200) {
                const newRow = { id: res.data.data.id, col1: res.data.data.name };

                dispatch({ type: hotelSettingsConstants.ADD_CANCELLATION_POLICY_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: hotelSettingsConstants.ADD_CANCELLATION_POLICY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.ADD_CANCELLATION_POLICY_ERROR, payload: error });
        }
    };
};

export const getCancellationPolicy = () => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.GET_CANCELLATION_POLICY_REQUEST });
        try {
            const res = await client.get('bolsa/Hotel/cancellationpolicy');

            if (res.status === 200) {
                dispatch({ type: hotelSettingsConstants.GET_CANCELLATION_POLICY_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: hotelSettingsConstants.GET_CANCELLATION_POLICY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.GET_CANCELLATION_POLICY_ERROR, payload: error });
        }
    };
};

export const deleteCancellationPolicy = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_REQUEST });
        try {
            const res = await client.delete(`bolsa/Hotel/cancellationpolicy/${id}`);

            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_SUCCESS, payload: id });
            } else {
                dispatch({ type: hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_ERROR, payload: error });
        }
    };
};

export const updateCancellationPolicy = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_REQUEST });
        try {
            const res = await client.put(`bolsa/Hotel/cancellationpolicy/${obj.id}`, {
                name: obj.col1
            });
            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_SUCCESS, payload: obj });
            } else {
                dispatch({ type: hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_ERROR, payload: error });
        }
    };
};

export const addCancellationPolicyItem = (day, refund, cancellationPolicyId) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.ADD_CANCELLATION_POLICY_ITEM_REQUEST });
        try {
            const res = await client.post('bolsa/Hotel/cancellationpolicyitem', { day, refund, cancellationPolicyId });

            if (res.status === 200) {
                const newRow = {
                    id: res.data.data.id,
                    col1: res.data.data.day,
                    col2: res.data.data.refund
                };

                dispatch({ type: hotelSettingsConstants.ADD_CANCELLATION_POLICY_ITEM_SUCCESS, payload: newRow });
            } else {
                dispatch({ type: hotelSettingsConstants.ADD_CANCELLATION_POLICY_ITEM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.ADD_CANCELLATION_POLICY_ITEM_ERROR, payload: error });
        }
    };
};

export const getCancellationPolicyItem = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.GET_CANCELLATION_POLICY_ITEM_REQUEST });
        try {
            const res = await client.get(`bolsa/Hotel/cancellationpolicy/${id}`);

            if (res.status === 200) {
                dispatch({ type: hotelSettingsConstants.GET_CANCELLATION_POLICY_ITEM_SUCCESS, payload: res.data.data.child });
            } else {
                dispatch({ type: hotelSettingsConstants.GET_CANCELLATION_POLICY_ITEM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.GET_CANCELLATION_POLICY_ITEM_ERROR, payload: error });
        }
    };
};

export const deleteCancellationPolicyItem = (id) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_ITEM_REQUEST });
        try {
            const res = await client.delete(`bolsa/Hotel/cancellationpolicyitem/${id}`);

            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_ITEM_SUCCESS, payload: id });
            } else {
                dispatch({ type: hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_ITEM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_ITEM_ERROR, payload: error });
        }
    };
};

export const updateCancellationPolicyItem = (obj) => {
    return async (dispatch) => {
        dispatch({ type: hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_ITEM_REQUEST });
        try {
            const res = await client.put(`bolsa/Hotel/cancellationpolicyitem/${obj.id}`, {
                day: obj.col1,
                refund: obj.col2
            });
            if (res.status === 204) {
                dispatch({ type: hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_ITEM_SUCCESS, payload: obj });
            } else {
                dispatch({ type: hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_ITEM_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_ITEM_ERROR, payload: error });
        }
    };
};
