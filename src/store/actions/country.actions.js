import { countryConstants } from 'store/constants';
import { client } from '_helpers/axios-settings';

export const getCountry = () => {
    return async (dispatch) => {
        dispatch({ type: countryConstants.GET_COUNTRY_REQUEST });
        try {
            const res = await client.get('bolsa/Country/GetCountry');

            if (res.status === 200) {
                dispatch({ type: countryConstants.GET_COUNTRY_SUCCESS, payload: res.data.data });
            } else {
                dispatch({ type: countryConstants.GET_COUNTRY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: countryConstants.GET_COUNTRY_ERROR, payload: error });
        }
    };
};

export const getCityByCountryId = (id) => {
    return async (dispatch) => {
        dispatch({ type: countryConstants.GET_CITY_BY_COUNTRY_REQUEST });
        try {
            const res = await client.get(`bolsa/Country/GetCity/${id}`);

            if (res.status === 200) {
                dispatch({ type: countryConstants.GET_CITY_BY_COUNTRY_SUCCESS, payload: res.data.data.child });
            } else {
                dispatch({ type: countryConstants.GET_CITY_BY_COUNTRY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: countryConstants.GET_CITY_BY_COUNTRY_ERROR, payload: error });
        }
    };
};

export const getDistrictByCityId = (id) => {
    return async (dispatch) => {
        dispatch({ type: countryConstants.GET_DISTRICT_BY_CITY_REQUEST });
        try {
            const res = await client.get(`bolsa/Country/District/${id}`);

            if (res.status === 200) {
                dispatch({ type: countryConstants.GET_DISTRICT_BY_CITY_SUCCESS, payload: res.data.data.child });
            } else {
                dispatch({ type: countryConstants.GET_DISTRICT_BY_CITY_ERROR, payload: res.error });
            }
        } catch (error) {
            dispatch({ type: countryConstants.GET_DISTRICT_BY_CITY_ERROR, payload: error });
        }
    };
};
