import { combineReducers } from 'redux';
import authentication from './authentication.reducer';
import customization from './customize.reducer';
import currency from './currency.reducer';
import country from './country.reducer';
import hotel from './hotel.reducer';
import hotelsettings from './hotelsettings.reducer';
import paymentType from './payment.reducer';
import toursettings from './toursettings.reducer';

const rootReducer = combineReducers({
    authentication,
    customization,
    currency,
    country,
    hotel,
    hotelsettings,
    paymentType,
    toursettings
});

export default rootReducer;
