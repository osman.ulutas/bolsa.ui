import { hotelConstants } from 'store/constants/';

const initialState = {
    loading: false,
    oneItemFetched: false,
    oneLoading: false,
    error: '',
    inserted: false,
    updated: false,
    remeoved: false,
    items: [],
    oneItem: {},
    rows: [],
    child_policy_rows: [],
    cancellation_policy_rows: [],
    room_type_rows: [],
    contract_rows: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case hotelConstants.ADD_HOTEL_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };
        case hotelConstants.ADD_HOTEL_SUCCESS:
            return {
                ...state,
                loading: false,
                inserted: true,
                updated: false,
                error: '',
                items: [...state.items, action.payload],
                rows: [...state.rows, action.payload]
            };
        case hotelConstants.ADD_HOTEL_ERROR:
            return {
                loading: false,
                updated: false,
                error: action.payload
            };
        case hotelConstants.GET_HOTEL_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };
        case hotelConstants.GET_HOTEL_SUCCESS:
            return {
                ...state,
                loading: false,
                updated: false,
                error: '',
                items: action.payload,
                rows: action.payload.map((hotel) => {
                    return { id: hotel.id, col1: hotel.name, col2: hotel.status, col3: hotel.city.name, col4: hotel.phone };
                })
            };
        case hotelConstants.GET_HOTEL_ERROR:
            return {
                loading: false,
                error: action.payload
            };
        case hotelConstants.REMOVE_HOTEL_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };
        case hotelConstants.REMOVE_HOTEL_SUCCESS:
            return {
                ...state,
                loading: false,
                updated: false,
                error: '',
                remeoved: true,
                items: state.items.filter((hotel) => {
                    return hotel.id !== action.payload;
                }),
                rows: state.rows.filter((hotel) => {
                    return hotel.id !== action.payload;
                })
            };
        case hotelConstants.REMOVE_HOTEL_ERROR:
            return {
                loading: false,
                updated: false,
                error: action.payload
            };
        case hotelConstants.UPDATE_HOTEL_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };
        case hotelConstants.UPDATE_HOTEL_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
                updated: true,
                items: state.items.map((hotel) => {
                    if (hotel.id === action.payload.id) {
                        return { ...hotel, hotels: action.payload.col1 };
                    }
                    return hotel;
                }),
                rows: state.rows.map((hotel) => {
                    if (hotel.id === action.payload.id) {
                        return { ...hotel, ...action.payload };
                    }
                    return hotel;
                }),
                oneItem: { ...state.oneItem, ...action.payload }
            };
        case hotelConstants.UPDATE_HOTEL_ERROR:
            return {
                loading: false,
                updated: false,
                error: action.payload
            };

        case hotelConstants.GET_ONE_HOTEL_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };
        case hotelConstants.GET_ONE_HOTEL_SUCCESS:
            return {
                ...state,
                error: '',
                updated: false,
                oneItem: action.payload,
                child_policy_rows: action.payload.childPolicy.map((child_policy_item) => {
                    return {
                        id: child_policy_item.id,
                        col1: child_policy_item.name,
                        col2: child_policy_item.minAge,
                        col3: child_policy_item.maxAge,
                        col4: child_policy_item.discount
                    };
                }),
                cancellation_policy_rows: action.payload.cancellationPolicy.map((cancellation_policy) => {
                    return { id: cancellation_policy.id, col1: cancellation_policy.name };
                }),
                room_type_rows: action.payload.hotelRoomType.map((room_type) => {
                    return { id: room_type.id, col1: room_type.name, col2: room_type.child, col3: room_type.adult };
                }),
                contract_rows: action.payload.hotelContracts.map((contract) => {
                    let obj = { id: '', col1: '', col2: '', col3: '', col4: '', col5: '', col6: '', col7: '', col8: '' };
                    obj = {
                        id: contract.id,
                        col1: contract.startDate,
                        col2: contract.endDate
                    };
                    const arrObj = contract.hotelRoomPrice.map((el, index) => {
                        if (el.name === 'Single') {
                            obj.col3 = el.price;
                            obj.col6 = el.id;
                        } else if (el.name === 'Double') {
                            obj.col4 = el.price;
                            obj.col7 = el.id;
                        } else if (el.name === 'Triple') {
                            obj.col5 = el.price;
                            obj.col8 = el.id;
                        }
                        return obj;
                    });

                    return { ...arrObj[arrObj.length - 1] };
                }),
                oneItemFetched: true,
                oneLoading: false,
                loading: false
            };
        case hotelConstants.GET_ONE_HOTEL_ERROR:
            return {
                oneLoading: false,
                updated: false,
                error: action.payload
            };

        case hotelConstants.ADD_HOTEL_ROOM_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };
        case hotelConstants.ADD_HOTEL_ROOM_SUCCESS:
            return {
                ...state,
                error: '',
                updated: false,
                room_type_rows: [...state.room_type_rows, action.payload],
                oneItemFetched: true,
                oneLoading: false,
                loading: false
            };
        case hotelConstants.ADD_HOTEL_ROOM_ERROR:
            return {
                oneLoading: false,
                updated: false,
                error: action.payload
            };

        case hotelConstants.ADD_HOTEL_CILD_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };
        case hotelConstants.ADD_HOTEL_CHILD_SUCCESS:
            return {
                ...state,
                updated: false,
                error: '',
                child_policy_rows: [...state.child_policy_rows, action.payload],
                oneItemFetched: true,
                oneLoading: false,
                loading: false
            };
        case hotelConstants.ADD_HOTEL_CHILD_ERROR:
            return {
                oneLoading: false,
                updated: false,
                error: action.payload
            };

        case hotelConstants.ADD_HOTEL_CANCELLATION_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };
        case hotelConstants.ADD_HOTEL_CANCELLATION_SUCCESS:
            return {
                ...state,
                error: '',
                updated: false,
                cancellation_policy_rows: [...state.cancellation_policy_rows, action.payload],
                oneItemFetched: true,
                oneLoading: false,
                loading: false
            };
        case hotelConstants.ADD_HOTEL_CANCELLATION_ERROR:
            return {
                oneLoading: false,
                updated: false,
                error: action.payload
            };
        case hotelConstants.ADD_HOTEL_CONTRACT_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };
        case hotelConstants.ADD_HOTEL_CONTRACT_SUCCESS:
            return {
                ...state,
                error: '',
                updated: false,
                contract_rows: [...state.contract_rows, action.payload],
                oneItemFetched: true,
                oneLoading: false,
                loading: false
            };
        case hotelConstants.ADD_HOTEL_CONTRACT_ERROR:
            return {
                oneLoading: false,
                updated: false,
                error: action.payload
            };
        case hotelConstants.REMOVE_HOTEL_CANCELLATION_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };
        case hotelConstants.REMOVE_HOTEL_CANCELLATION_SUCCESS:
            return {
                ...state,
                updated: false,
                error: '',
                cancellation_policy_rows: state.cancellation_policy_rows.filter((cancel) => {
                    return cancel.id !== action.payload;
                }),
                oneItemFetched: true,
                oneLoading: false,
                loading: false
            };
        case hotelConstants.REMOVE_HOTEL_CILD_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };
        case hotelConstants.REMOVE_HOTEL_CHILD_SUCCESS:
            return {
                ...state,
                error: '',
                updated: false,
                child_policy_rows: state.child_policy_rows.filter((child) => {
                    return child.id !== action.payload;
                }),
                oneItemFetched: true,
                oneLoading: false,
                loading: false
            };
        case hotelConstants.REMOVE_HOTEL_CHILD_ERROR:
            return {
                oneLoading: false,
                updated: false,
                error: action.payload
            };

        case hotelConstants.REMOVE_HOTEL_ROOM_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };

        case hotelConstants.REMOVE_HOTEL_ROOM_SUCCESS:
            return {
                ...state,
                updated: false,
                error: '',
                room_type_rows: state.room_type_rows.filter((room) => {
                    return room.id !== action.payload;
                }),
                oneItemFetched: true,
                oneLoading: false,
                loading: false
            };
        case hotelConstants.REMOVE_HOTEL_ROOM_ERROR:
            return {
                oneLoading: false,
                updated: false,
                error: action.payload
            };

        case hotelConstants.REMOVE_HOTEL_CONTRACT_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };

        case hotelConstants.REMOVE_HOTEL_CONTRACT_SUCCESS:
            return {
                ...state,
                updated: false,
                error: '',
                contract_rows: state.contract_rows.filter((contract) => {
                    return contract.id !== action.payload;
                }),
                oneItemFetched: true,
                oneLoading: false,
                loading: false
            };
        case hotelConstants.REMOVE_HOTEL_CONTRACT_ERROR:
            return {
                oneLoading: false,
                updated: false,
                error: action.payload
            };

        case hotelConstants.UPDATE_HOTEL_CONTRACT_REQUEST:
            return {
                ...state,
                updated: false,
                loading: true
            };
        case hotelConstants.UPDATE_HOTEL_CONTRACT_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
                updated: true,
                contract_rows: state.contract_rows.map((contract) => {
                    if (contract.id === action.payload.id) {
                        return { ...contract, ...action.payload };
                    }
                    return contract;
                })
            };
        case hotelConstants.UPDATE_HOTEL_CONTRACT_ERROR:
            return {
                loading: false,
                updated: false,
                error: action.payload
            };

        default:
            return state;
    }
};
