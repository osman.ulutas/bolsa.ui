import { countryConstants } from 'store/constants/';

const initialState = {
    country_loading: false,
    city_by_country_loading: false,
    country_fetched: false,
    city_fetched: false,
    district_fetched: false,
    district_start_fetched: false,
    district_by_city_loading: false,
    error: '',
    country_items: [],
    country_rows: [],
    city_by_country_items: [],
    city_by_country_rows: [],
    district_by_city_items: [],
    district_by_city_rows: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case countryConstants.GET_COUNTRY_REQUEST:
            return {
                ...state,
                city_fetched: false,
                country_fetched: false,
                city_fetched_finish: false,
                district_fetched: false,
                district_start_fetched: false,
                country_loading: true
            };
        case countryConstants.GET_COUNTRY_SUCCESS:
            return {
                ...state,
                country_loading: false,
                error: '',
                city_by_country_items: [],
                city_by_country_rows: [],
                district_by_city_items: [],
                district_by_city_rows: [],
                country_items: action.payload,
                country_rows: action.payload.map((country) => {
                    return { id: country.id, col1: country.name };
                }),
                country_fetched: true
            };

        case countryConstants.GET_COUNTRY_ERROR:
            return {
                country_loading: false,
                error: action.payload
            };

        case countryConstants.GET_CITY_BY_COUNTRY_REQUEST:
            return {
                ...state,
                city_by_country_loading: true,
                city_fetched: false,
                city_fetched_finish: false,
                district_fetched: false,
                district_start_fetched: false
            };
        case countryConstants.GET_CITY_BY_COUNTRY_SUCCESS:
            return {
                ...state,
                city_fetched: true,
                city_by_country_loading: false,
                city_fetched_finish: true,
                district_start_fetched: true,
                error: '',
                district_by_city_items: [],
                district_by_city_rows: [],
                city_by_country_items: action.payload,
                city_by_country_rows: action.payload.map((city_by_country) => {
                    return { id: city_by_country.id, col1: city_by_country.name };
                })
            };
        case countryConstants.GET_CITY_BY_COUNTRY_ERROR:
            return {
                city_by_country_loading: false,
                error: action.payload
            };

        case countryConstants.GET_DISTRICT_BY_CITY_REQUEST:
            return {
                ...state,
                city_fetched_finish: false,
                district_start_fetched: false,
                district_by_city_loading: false
            };
        case countryConstants.GET_DISTRICT_BY_CITY_SUCCESS:
            return {
                ...state,
                district_fetched: true,
                district_start_fetched: false,
                district_by_city_loading: false,
                error: '',
                district_by_city_items: action.payload,
                district_by_city_rows: action.payload.map((district_by_city) => {
                    return { id: district_by_city.id, col1: district_by_city.name };
                })
            };
        case countryConstants.GET_DISTRICT_BY_CITY_ERROR:
            return {
                district_by_city_loading: false,
                error: action.payload
            };

        default:
            return state;
    }
};
