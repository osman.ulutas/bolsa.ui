import { hotelSettingsConstants } from 'store/constants/';

const initialState = {
    star_loading: false,
    room_type_loading: false,
    child_policy_item_loading: false,
    child_policy_loading: false,
    cancellation_policy_loading: false,
    cancellation_policy_item_loading: false,
    category_policy_loading: false,
    star_items: [],
    room_type_items: [],
    child_policy_item_items: [],
    child_policy_items: [],
    cancellation_policy_item_items: [],
    category_policy_items: [],
    star_rows: [],
    room_type_rows: [],
    child_policy_item_rows: [],
    child_policy_rows: [],
    cancellation_policy_rows: [],
    cancellation_policy_item_rows: [],
    category_policy_rows: [],
    error: ''
};
export default (state = initialState, action) => {
    switch (action.type) {
        // =============================|| STAR REDUCER ||============================= //
        case hotelSettingsConstants.ADD_STAR_REQUEST:
            return {
                ...state,
                star_loading: true
            };
        case hotelSettingsConstants.ADD_STAR_SUCCESS:
            return {
                ...state,
                star_loading: false,
                error: '',
                star_items: [...state.star_items, action.payload],
                star_rows: [...state.star_rows, action.payload]
            };
        case hotelSettingsConstants.ADD_STAR_ERROR:
            return {
                star_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.GET_STAR_REQUEST:
            return {
                ...state,
                star_loading: true
            };
        case hotelSettingsConstants.GET_STAR_SUCCESS:
            return {
                ...state,
                star_loading: false,
                error: '',
                inserted: true,
                star_items: action.payload,
                star_rows: action.payload.map((star) => {
                    return { id: star.id, col1: star.stars };
                })
            };
        case hotelSettingsConstants.GET_STAR_ERROR:
            return {
                star_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.REMOVE_STAR_REQUEST:
            return {
                ...state,
                star_loading: true
            };
        case hotelSettingsConstants.REMOVE_STAR_SUCCESS:
            return {
                ...state,
                star_loading: false,
                error: '',
                remeoved: true,
                star_items: state.star_items.filter((star) => {
                    return star.id !== action.payload;
                }),
                star_rows: state.star_rows.filter((star) => {
                    return star.id !== action.payload;
                })
            };
        case hotelSettingsConstants.REMOVE_STAR_ERROR:
            return {
                star_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.UPDATE_STAR_REQUEST:
            return {
                ...state,
                star_loading: true
            };
        case hotelSettingsConstants.UPDATE_STAR_SUCCESS:
            return {
                ...state,
                star_loading: false,
                error: '',
                remeoved: true,
                star_items: state.star_items.map((star) => {
                    if (star.id === action.payload.id) {
                        return { ...star, stars: action.payload.col1 };
                    }
                    return star;
                }),
                star_rows: state.star_rows.map((star) => {
                    if (star.id === action.payload.id) {
                        return { ...star, ...action.payload };
                    }
                    return star;
                })
            };
        case hotelSettingsConstants.UPDATE_STAR_ERROR:
            return {
                star_loading: false,
                error: action.payload
            };
        // =============================|| END REDUCER ||============================= //
        // =============================|| CATEGORY REDUCER ||============================= //

        case hotelSettingsConstants.ADD_CATEGORY_REQUEST:
            return {
                ...state,
                category_policy_loading: true
            };
        case hotelSettingsConstants.ADD_CATEGORY_SUCCESS:
            return {
                ...state,
                category_policy_loading: false,
                error: '',
                category_policy_items: [...state.category_policy_items, action.payload],
                category_policy_rows: [...state.category_policy_rows, action.payload]
            };
        case hotelSettingsConstants.ADD_CATEGORY_ERROR:
            return {
                category_policy_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.GET_CATEGORY_REQUEST:
            return {
                ...state,
                category_policy_loading: true
            };
        case hotelSettingsConstants.GET_CATEGORY_SUCCESS:
            return {
                ...state,
                category_policy_loading: false,
                error: '',
                inserted: true,
                category_policy_items: action.payload,
                category_policy_rows: action.payload.map((category_policy) => {
                    return { id: category_policy.id, col1: category_policy.name };
                })
            };
        case hotelSettingsConstants.GET_CATEGORY_ERROR:
            return {
                category_policy_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.REMOVE_CATEGORY_REQUEST:
            return {
                ...state,
                category_policy_loading: true
            };
        case hotelSettingsConstants.REMOVE_CATEGORY_SUCCESS:
            return {
                ...state,
                category_policy_loading: false,
                error: '',
                remeoved: true,
                category_policy_items: state.category_policy_items.filter((category_policy) => {
                    return category_policy.id !== action.payload;
                }),
                category_policy_rows: state.category_policy_rows.filter((category_policy) => {
                    return category_policy.id !== action.payload;
                })
            };
        case hotelSettingsConstants.REMOVE_CATEGORY_ERROR:
            return {
                category_policy_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.UPDATE_CATEGORY_REQUEST:
            return {
                ...state,
                category_policy_loading: true
            };
        case hotelSettingsConstants.UPDATE_CATEGORY_SUCCESS:
            return {
                ...state,
                category_policy_loading: false,
                error: '',
                remeoved: true,
                category_policy_items: state.category_policy_items.map((category_policy) => {
                    if (category_policy.id === action.payload.id) {
                        return { ...category_policy, name: action.payload.col1 };
                    }
                    return category_policy;
                }),
                category_policy_rows: state.category_policy_rows.map((category_policy) => {
                    if (category_policy.id === action.payload.id) {
                        return { ...category_policy, ...action.payload };
                    }
                    return category_policy;
                })
            };
        case hotelSettingsConstants.UPDATE_CATEGORY_ERROR:
            return {
                category_policy_loading: false,
                error: action.payload
            };
        // =============================|| END CATEGORY ||============================= //
        // =============================||  ROOM TYPE REDUCER ||============================= //

        case hotelSettingsConstants.ADD_ROOM_TYPE_REQUEST:
            return {
                ...state,
                room_type_loading: true
            };
        case hotelSettingsConstants.ADD_ROOM_TYPE_SUCCESS:
            return {
                ...state,
                room_type_loading: false,
                error: '',
                room_type_items: [...state.room_type_items, action.payload],
                room_type_rows: [...state.room_type_rows, action.payload]
            };
        case hotelSettingsConstants.ADD_ROOM_TYPE_ERROR:
            return {
                room_type_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.GET_ROOM_TYPE_REQUEST:
            return {
                ...state,
                room_type_loading: true
            };
        case hotelSettingsConstants.GET_ROOM_TYPE_SUCCESS:
            return {
                ...state,
                room_type_loading: false,
                error: '',
                inserted: true,
                room_type_items: action.payload,
                room_type_rows: action.payload.map((room_type) => {
                    return { id: room_type.id, col1: room_type.name, col2: room_type.child, col3: room_type.adult };
                })
            };
        case hotelSettingsConstants.GET_ROOM_TYPE_ERROR:
            return {
                room_type_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.REMOVE_ROOM_TYPE_REQUEST:
            return {
                ...state,
                room_type_loading: true
            };
        case hotelSettingsConstants.REMOVE_ROOM_TYPE_SUCCESS:
            return {
                ...state,
                room_type_loading: false,
                error: '',
                remeoved: true,
                room_type_items: state.room_type_items.filter((room_type) => {
                    return room_type.id !== action.payload;
                }),
                room_type_rows: state.room_type_rows.filter((room_type) => {
                    return room_type.id !== action.payload;
                })
            };
        case hotelSettingsConstants.REMOVE_ROOM_TYPE_ERROR:
            return {
                room_type_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.UPDATE_ROOM_TYPE_REQUEST:
            return {
                ...state,
                room_type_loading: true
            };
        case hotelSettingsConstants.UPDATE_ROOM_TYPE_SUCCESS:
            return {
                ...state,
                room_type_loading: false,
                error: '',
                remeoved: true,
                room_type_items: state.room_type_items.map((room_type) => {
                    if (room_type.id === action.payload.id) {
                        return { ...room_type, name: action.payload.col1, adult: action.payload.col3, child: action.payload.col2 };
                    }
                    return room_type;
                }),
                room_type_rows: state.room_type_rows.map((room_type) => {
                    if (room_type.id === action.payload.id) {
                        return { ...room_type, ...action.payload };
                    }
                    return room_type;
                })
            };
        case hotelSettingsConstants.UPDATE_ROOM_TYPE_ERROR:
            return {
                room_type_loading: false,
                error: action.payload
            };
        // =============================||  END ROOM TYPE REDUCER ||============================= //
        // =============================||   CHILD ITEM POLICY REDUCER ||============================= //

        case hotelSettingsConstants.ADD_CHILD_POLICY_ITEM_REQUEST:
            return {
                ...state,
                child_policy_item_loading: true
            };
        case hotelSettingsConstants.ADD_CHILD_POLICY_ITEM_SUCCESS:
            return {
                ...state,
                child_policy_item_loading: false,
                error: '',
                child_policy_item_items: [...state.child_policy_item_items, action.payload],
                child_policy_item_rows: [...state.child_policy_item_rows, action.payload]
            };
        case hotelSettingsConstants.ADD_CHILD_POLICY_ITEM_ERROR:
            return {
                child_policy_item_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.GET_CHILD_POLICY_ITEM_REQUEST:
            return {
                ...state,
                child_policy_item_loading: true
            };
        case hotelSettingsConstants.GET_CHILD_POLICY_ITEM_SUCCESS:
            return {
                ...state,
                child_policy_item_loading: false,
                error: '',
                inserted: true,
                child_policy_item_items: action.payload,
                child_policy_item_rows: action.payload.map((child_policy_item) => {
                    return {
                        id: child_policy_item.id,
                        col1: child_policy_item.name,
                        col2: child_policy_item.minAge,
                        col3: child_policy_item.maxAge,
                        col4: child_policy_item.discount
                    };
                })
            };
        case hotelSettingsConstants.GET_CHILD_POLICY_ITEM_ERROR:
            return {
                child_policy_item_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.REMOVE_CHILD_POLICY_ITEM_REQUEST:
            return {
                ...state,
                child_policy_item_loading: true
            };
        case hotelSettingsConstants.REMOVE_CHILD_POLICY_ITEM_SUCCESS:
            return {
                ...state,
                child_policy_item_loading: false,
                error: '',
                remeoved: true,
                child_policy_item_items: state.child_policy_item_items.filter((child_policy_item) => {
                    return child_policy_item.id !== action.payload;
                }),
                child_policy_item_rows: state.child_policy_item_rows.filter((child_policy_item) => {
                    return child_policy_item.id !== action.payload;
                })
            };
        case hotelSettingsConstants.REMOVE_CHILD_POLICY_ITEM_ERROR:
            return {
                child_policy_item_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.UPDATE_CHILD_POLICY_ITEM_REQUEST:
            return {
                ...state,
                child_policy_item_loading: true
            };
        case hotelSettingsConstants.UPDATE_CHILD_POLICY_ITEM_SUCCESS:
            return {
                ...state,
                child_policy_item_loading: false,
                error: '',
                remeoved: true,
                child_policy_item_items: state.child_policy_item_items.map((child_policy_item) => {
                    if (child_policy_item.id === action.payload.id) {
                        return {
                            ...child_policy_item,
                            name: action.payload.col1,
                            minAge: action.payload.minAge,
                            maxAge: action.payload.maxAge,
                            discount: action.payload.discount
                        };
                    }
                    return child_policy_item;
                }),
                child_policy_item_rows: state.child_policy_item_rows.map((child_policy_item) => {
                    if (child_policy_item.id === action.payload.id) {
                        return { ...child_policy_item, ...action.payload };
                    }
                    return child_policy_item;
                })
            };
        case hotelSettingsConstants.UPDATE_CHILD_POLICY_ITEM_ERROR:
            return {
                child_policy_item_loading: false,
                error: action.payload
            };

        // =============================||  END CHILD ITEM POLICY REDUCER ||============================= //
        // =============================||   CANCELLATION  POLICY REDUCER ||============================= //

        case hotelSettingsConstants.ADD_CANCELLATION_POLICY_REQUEST:
            return {
                ...state,
                cancellation_policy_loading: true
            };
        case hotelSettingsConstants.ADD_CANCELLATION_POLICY_SUCCESS:
            return {
                ...state,
                cancellation_policy_loading: false,
                error: '',
                cancellation_policy_items: [...state.cancellation_policy_items, action.payload],
                cancellation_policy_rows: [...state.cancellation_policy_rows, action.payload]
            };
        case hotelSettingsConstants.ADD_CANCELLATION_POLICY_ERROR:
            return {
                cancellation_policy_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.GET_CANCELLATION_POLICY_REQUEST:
            return {
                ...state,
                cancellation_policy_loading: true
            };
        case hotelSettingsConstants.GET_CANCELLATION_POLICY_SUCCESS:
            return {
                ...state,
                cancellation_policy_loading: false,
                error: '',
                inserted: true,
                cancellation_policy_items: action.payload,
                cancellation_policy_rows: action.payload.map((cancellation_policy) => {
                    return { id: cancellation_policy.id, col1: cancellation_policy.name };
                })
            };
        case hotelSettingsConstants.GET_CANCELLATION_POLICY_ERROR:
            return {
                cancellation_policy_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_REQUEST:
            return {
                ...state,
                cancellation_policy_loading: true
            };
        case hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_SUCCESS:
            return {
                ...state,
                cancellation_policy_loading: false,
                error: '',
                remeoved: true,
                cancellation_policy_items: state.cancellation_policy_items.filter((cancellation_policy) => {
                    return cancellation_policy.id !== action.payload;
                }),
                cancellation_policy_rows: state.cancellation_policy_rows.filter((cancellation_policy) => {
                    return cancellation_policy.id !== action.payload;
                })
            };
        case hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_ERROR:
            return {
                cancellation_policy_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_REQUEST:
            return {
                ...state,
                cancellation_policy_loading: true
            };
        case hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_SUCCESS:
            return {
                ...state,
                cancellation_policy_loading: false,
                error: '',
                remeoved: true,
                cancellation_policy_items: state.cancellation_policy_items.map((cancel_pol) => {
                    if (cancel_pol.id === action.payload.id) {
                        return {
                            ...cancel_pol,
                            name: action.payload.col1
                        };
                    }
                    return cancel_pol;
                }),
                cancellation_policy_rows: state.cancellation_policy_rows.map((cancel_pol) => {
                    if (cancel_pol.id === action.payload.id) {
                        return { ...cancel_pol, ...action.payload };
                    }
                    return cancel_pol;
                })
            };
        case hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_ERROR:
            return {
                cancellation_policy_loading: false,
                error: action.payload
            };

        // =============================||  END CANCELLATION POLICY REDUCER ||============================= //
        // =============================||  EEND CHILD POLICY REDUCER ||============================= //

        case hotelSettingsConstants.ADD_CHILD_POLICY_REQUEST:
            return {
                ...state,
                child_policy_loading: true
            };
        case hotelSettingsConstants.ADD_CHILD_POLICY_SUCCESS:
            return {
                ...state,
                child_policy_loading: false,
                error: '',
                child_policy_items: [...state.child_policy_items, action.payload],
                child_policy_rows: [...state.child_policy_rows, action.payload]
            };
        case hotelSettingsConstants.ADD_CHILD_POLICY_ERROR:
            return {
                child_policy_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.GET_CHILD_POLICY_REQUEST:
            return {
                ...state,
                child_policy_loading: true
            };
        case hotelSettingsConstants.GET_CHILD_POLICY_SUCCESS:
            return {
                ...state,
                child_policy_loading: false,
                error: '',
                inserted: true,
                child_policy_items: action.payload,
                child_policy_rows: action.payload.map((child_policy) => {
                    return { id: child_policy.id, col1: child_policy.name };
                })
            };
        case hotelSettingsConstants.GET_CHILD_POLICY_ERROR:
            return {
                child_policy_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.REMOVE_CHILD_POLICY_REQUEST:
            return {
                ...state,
                child_policy_loading: true
            };
        case hotelSettingsConstants.REMOVE_CHILD_POLICY_SUCCESS:
            return {
                ...state,
                child_policy_loading: false,
                error: '',
                remeoved: true,
                child_policy_items: state.child_policy_items.filter((child_policy) => {
                    return child_policy.id !== action.payload;
                }),
                child_policy_rows: state.child_policy_rows.filter((child_policy) => {
                    return child_policy.id !== action.payload;
                })
            };
        case hotelSettingsConstants.REMOVE_CHILD_POLICY_ERROR:
            return {
                child_policy_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.UPDATE_CHILD_POLICY_REQUEST:
            return {
                ...state,
                child_policy_loading: true
            };
        case hotelSettingsConstants.UPDATE_CHILD_POLICY_SUCCESS:
            return {
                ...state,
                child_policy_loading: false,
                error: '',
                remeoved: true,
                child_policy_items: state.child_policy_items.map((cancel_pol) => {
                    if (cancel_pol.id === action.payload.id) {
                        return {
                            ...cancel_pol,
                            name: action.payload.col1
                        };
                    }
                    return cancel_pol;
                }),
                child_policy_rows: state.child_policy_rows.map((cancel_pol) => {
                    if (cancel_pol.id === action.payload.id) {
                        return { ...cancel_pol, ...action.payload };
                    }
                    return cancel_pol;
                })
            };
        case hotelSettingsConstants.UPDATE_CHILD_POLICY_ERROR:
            return {
                child_policy_loading: false,
                error: action.payload
            };
        // =============================||  EEND CHILD POLICY REDUCER ||============================= //
        // =============================||  CANCELLATION ITEM POLICY REDUCER ||============================= //

        case hotelSettingsConstants.ADD_CANCELLATION_POLICY_ITEM_REQUEST:
            return {
                ...state,
                cancellation_policy_item_loading: true
            };
        case hotelSettingsConstants.ADD_CANCELLATION_POLICY_ITEM_SUCCESS:
            return {
                ...state,
                cancellation_policy_item_loading: false,
                error: '',
                cancellation_policy_item_items: [...state.cancellation_policy_item_items, action.payload],
                cancellation_policy_item_rows: [...state.cancellation_policy_item_rows, action.payload]
            };
        case hotelSettingsConstants.ADD_CANCELLATION_POLICY_ITEM_ERROR:
            return {
                cancellation_policy_item_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.GET_CANCELLATION_POLICY_ITEM_REQUEST:
            return {
                ...state,
                cancellation_policy_item_loading: true
            };
        case hotelSettingsConstants.GET_CANCELLATION_POLICY_ITEM_SUCCESS:
            return {
                ...state,
                cancellation_policy_item_loading: false,
                error: '',
                inserted: true,
                cancellation_policy_item_items: action.payload,
                cancellation_policy_item_rows: action.payload.map((cancellation_pol) => {
                    return { id: cancellation_pol.id, col1: cancellation_pol.day, col2: cancellation_pol.refund };
                })
            };
        case hotelSettingsConstants.GET_CANCELLATION_POLICY_ITEM_ERROR:
            return {
                cancellation_policy_item_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_ITEM_REQUEST:
            return {
                ...state,
                cancellation_policy_item_loading: true
            };
        case hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_ITEM_SUCCESS:
            return {
                ...state,
                cancellation_policy_item_loading: false,
                error: '',
                remeoved: true,
                cancellation_policy_item_items: state.cancellation_policy_item_items.filter((cancellation_policy_item) => {
                    return cancellation_policy_item.id !== action.payload;
                }),
                cancellation_policy_item_rows: state.cancellation_policy_item_rows.filter((cancellation_policy_item) => {
                    return cancellation_policy_item.id !== action.payload;
                })
            };
        case hotelSettingsConstants.REMOVE_CANCELLATION_POLICY_ITEM_ERROR:
            return {
                cancellation_policy_item_loading: false,
                error: action.payload
            };
        case hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_ITEM_REQUEST:
            return {
                ...state,
                cancellation_policy_item_loading: true
            };
        case hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_ITEM_SUCCESS:
            return {
                ...state,
                cancellation_policy_item_loading: false,
                error: '',
                remeoved: true,
                cancellation_policy_item_items: state.cancellation_policy_item_items.map((cancel_pol) => {
                    if (cancel_pol.id === action.payload.id) {
                        return {
                            ...cancel_pol,
                            day: action.payload.col1,
                            refund: action.payload.col2
                        };
                    }
                    return cancel_pol;
                }),
                cancellation_policy_item_rows: state.cancellation_policy_item_rows.map((cancel_pol) => {
                    if (cancel_pol.id === action.payload.id) {
                        return { ...cancel_pol, ...action.payload };
                    }
                    return cancel_pol;
                })
            };
        case hotelSettingsConstants.UPDATE_CANCELLATION_POLICY_ITEM_ERROR:
            return {
                cancellation_policy_item_loading: false,
                error: action.payload
            };
        // =============================||  END CANCELLATION ITEM POLICY REDUCER ||============================= //

        default:
            return state;
    }
};
