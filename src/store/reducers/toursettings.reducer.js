import { tourSettingsConstants } from 'store/constants/';

const initialState = {
    travel_location_loading: false,
    travel_location_loading_curr: false,
    error: '',
    inserted: false,
    remeoved: false,
    travel_location_items: [],
    travel_location_rows: [],
    travel_location_curr_items: [],
    travel_location_curr_rows: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case tourSettingsConstants.ADD_TS_TRAVELLOCATION_REQUEST:
            return {
                ...state,
                travel_location_loading: true
            };
        case tourSettingsConstants.ADD_TS_TRAVELLOCATION_SUCCESS:
            return {
                ...state,
                travel_location_loading: false,
                error: '',
                travel_location_items: [...state.travel_location_items, action.payload],
                travel_location_rows: [...state.travel_location_rows, action.payload]
            };
        case tourSettingsConstants.ADD_TS_TRAVELLOCATION_ERROR:
            return {
                travel_location_loading: false,
                error: action.payload
            };
        case tourSettingsConstants.GET_TS_TRAVELLOCATION_REQUEST:
            return {
                ...state,
                travel_location_loading: true
            };
        case tourSettingsConstants.GET_TS_TRAVELLOCATION_SUCCESS:
            return {
                ...state,
                travel_location_loading: false,
                error: '',
                inserted: true,
                travel_location_items: action.payload,
                travel_location_rows: action.payload.map((travelloc) => {
                    return {
                        id: travelloc.id,
                        col1: travelloc.name,
                        col2: travelloc.country.name,
                        col3: travelloc.city.name,
                        col4: travelloc.distriction.name,
                        col5: travelloc.countryId,
                        col6: travelloc.cityId,
                        col7: travelloc.districtionId
                    };
                })
            };
        case tourSettingsConstants.GET_TS_TRAVELLOCATION_ERROR:
            return {
                travel_location_loading: false,
                error: action.payload
            };
        case tourSettingsConstants.REMOVE_TS_TRAVELLOCATION_REQUEST:
            return {
                ...state,
                travel_location_loading: true
            };
        case tourSettingsConstants.REMOVE_TS_TRAVELLOCATION_SUCCESS:
            return {
                ...state,
                travel_location_loading: false,
                error: '',
                remeoved: true,
                travel_location_items: state.travel_location_items.filter((travelloc) => {
                    return travelloc.id !== action.payload;
                }),
                travel_location_rows: state.travel_location_rows.filter((travelloc) => {
                    return travelloc.id !== action.payload;
                })
            };
        case tourSettingsConstants.REMOVE_TS_TRAVELLOCATION_ERROR:
            return {
                travel_location_loading: false,
                error: action.payload
            };
        case tourSettingsConstants.UPDATE_TS_TRAVELLOCATION_REQUEST:
            return {
                ...state,
                travel_location_loading: true
            };
        case tourSettingsConstants.UPDATE_TS_TRAVELLOCATION_SUCCESS:
            return {
                ...state,
                travel_location_loading: false,
                error: '',
                remeoved: true,
                travel_location_items: state.travel_location_items.map((travelloc) => {
                    if (travelloc.id === action.payload.id) {
                        return { ...travelloc, name: action.payload.name };
                    }
                    return travelloc;
                }),
                travel_location_rows: state.travel_location_rows.map((curr) => {
                    if (curr.id === action.payload.id) {
                        return { ...curr, ...action.payload };
                    }
                    return curr;
                })
            };
        case tourSettingsConstants.UPDATE_TS_TRAVELLOCATION_ERROR:
            return {
                travel_location_loading: false,
                error: action.payload
            };

        case tourSettingsConstants.ADD_TS_TRAVELLOCATIONITEM_REQUEST:
            return {
                ...state,
                travel_location_item_loading: true
            };
        case tourSettingsConstants.ADD_TS_TRAVELLOCATIONITEM_SUCCESS:
            return {
                ...state,
                travel_location_item_loading: false,
                error: '',
                travel_location_item_items: [...state.travel_location_item_items, action.payload],
                travel_location_item_rows: [...state.travel_location_item_rows, action.payload]
            };
        case tourSettingsConstants.ADD_TS_TRAVELLOCATIONITEM_ERROR:
            return {
                travel_location_item_loading: false,
                error: action.payload
            };
        case tourSettingsConstants.GET_TS_TRAVELLOCATIONITEM_REQUEST:
            return {
                ...state,
                travel_location_item_loading: true
            };
        case tourSettingsConstants.GET_TS_TRAVELLOCATIONITEM_SUCCESS:
            return {
                ...state,
                travel_location_item_loading: false,
                error: '',
                inserted: true,
                travel_location_item_items: action.payload,
                travel_location_item_rows: action.payload.map((travelloc) => {
                    return {
                        id: travelloc.id,
                        col1: travelloc.name,
                        col2: travelloc.country.name,
                        col3: travelloc.city.name,
                        col4: travelloc.distriction.name,
                        col5: travelloc.countryId,
                        col6: travelloc.cityId,
                        col7: travelloc.districtionId,
                        col8: travelloc.canSalePart,
                        col9: travelloc.isOther,
                        col10: travelloc.paymentTypeId,
                        col11: travelloc.paymentType.name,
                        col12: travelloc.optionalStatus === 0 ? 'Default' : 'Optinal',
                        col13: travelloc.optionalStatus
                    };
                })
            };
        case tourSettingsConstants.GET_TS_TRAVELLOCATIONITEM_ERROR:
            return {
                travel_location_item_loading: false,
                error: action.payload
            };
        case tourSettingsConstants.REMOVE_TS_TRAVELLOCATIONITEM_REQUEST:
            return {
                ...state,
                travel_location_item_loading: true
            };
        case tourSettingsConstants.REMOVE_TS_TRAVELLOCATIONITEM_SUCCESS:
            return {
                ...state,
                travel_location_item_loading: false,
                error: '',
                remeoved: true,
                travel_location_item_items: state.travel_location_item_items.filter((travelloc) => {
                    return travelloc.id !== action.payload;
                }),
                travel_location_item_rows: state.travel_location_item_rows.filter((travelloc) => {
                    return travelloc.id !== action.payload;
                })
            };
        case tourSettingsConstants.REMOVE_TS_TRAVELLOCATIONITEM_ERROR:
            return {
                travel_location_item_loading: false,
                error: action.payload
            };
        case tourSettingsConstants.UPDATE_TS_TRAVELLOCATIONITEM_REQUEST:
            return {
                ...state,
                travel_location_item_loading: true
            };
        case tourSettingsConstants.UPDATE_TS_TRAVELLOCATIONITEM_SUCCESS:
            debugger;
            return {
                ...state,
                travel_location_item_loading: false,
                error: '',
                remeoved: true,
                travel_location_item_items: state.travel_location_item_items.map((travelloc) => {
                    if (travelloc.id === action.payload.id) {
                        return { ...travelloc, ...action.payload };
                    }
                    return travelloc;
                }),
                travel_location_item_rows: state.travel_location_item_rows.map((travelloc) => {
                    if (travelloc.id === action.payload.id) {
                        return {
                            ...travelloc,
                            col1: travelloc.name,
                            col2: travelloc.country.name,
                            col3: travelloc.city.name,
                            col4: travelloc.distriction.name,
                            col5: travelloc.countryId,
                            col6: travelloc.cityId,
                            col7: travelloc.districtionId,
                            col8: travelloc.canSalePart,
                            col9: travelloc.isOther,
                            col10: travelloc.paymentTypeId,
                            col11: travelloc.paymentType.name,
                            col12: travelloc.optionalStatus === 0 ? 'Default' : 'Optinal',
                            col13: travelloc.optionalStatus
                        };
                    }
                    return travelloc;
                })
            };
        case tourSettingsConstants.UPDATE_TS_TRAVELLOCATIONITEM_ERROR:
            return {
                travel_location_item_loading: false,
                error: action.payload
            };

        case tourSettingsConstants.ADD_TS_CURRENCY_REQUEST:
            return {
                ...state,
                travel_location_loading_curr: true
            };
        case tourSettingsConstants.ADD_TS_CURRENCY_SUCCESS:
            return {
                ...state,
                travel_location_loading_curr: false,
                error: '',
                travel_location_curr_items: [...state.travel_location_curr_items, action.payload],
                travel_location_curr_rows: [...state.travel_location_curr_rows, action.payload]
            };
        case tourSettingsConstants.ADD_TS_CURRENCY_ERROR:
            return {
                travel_location_loading_curr: false,
                error: action.payload
            };
        case tourSettingsConstants.GET_TS_CURRENCY_REQUEST:
            return {
                ...state,
                travel_location_loading_curr: true
            };
        case tourSettingsConstants.GET_TS_CURRENCY_SUCCESS:
            return {
                ...state,
                travel_location_loading_curr: false,
                error: '',
                inserted: true,
                travel_location_curr_items: action.payload,
                travel_location_curr_rows: action.payload.map((dat) => {
                    return { id: dat.id, col1: dat.currency.name, col2: dat.cost, col3: dat.price };
                })
            };
        case tourSettingsConstants.GET_TS_CURRENCY_ERROR:
            return {
                travel_location_loading_curr: false,
                error: action.payload
            };
        case tourSettingsConstants.REMOVE_TS_CURRENCY_REQUEST:
            return {
                ...state,
                travel_location_loading_curr: true
            };
        case tourSettingsConstants.REMOVE_TS_CURRENCY_SUCCESS:
            return {
                ...state,
                travel_location_loading_curr: false,
                error: '',
                remeoved: true,
                travel_location_curr_items: state.travel_location_curr_items.filter((currency) => {
                    return currency.id !== action.payload;
                }),
                travel_location_curr_rows: state.travel_location_curr_rows.filter((currency) => {
                    return currency.id !== action.payload;
                })
            };
        case tourSettingsConstants.REMOVE_TS_CURRENCY_ERROR:
            return {
                travel_location_loading_curr: false,
                error: action.payload
            };
        case tourSettingsConstants.UPDATE_TS_CURRENCY_REQUEST:
            return {
                ...state,
                travel_location_loading_curr: true
            };
        case tourSettingsConstants.UPDATE_TS_CURRENCY_SUCCESS:
            return {
                ...state,
                travel_location_loading_curr: false,
                error: '',
                remeoved: true,
                travel_location_curr_items: state.travel_location_curr_items.map((currency) => {
                    if (currency.id === action.payload.id) {
                        return { ...currency, name: action.payload.name, cost: action.payload.cost, price: action.payload.price };
                    }
                    return currency;
                }),
                travel_location_curr_rows: state.travel_location_curr_rows.map((curr) => {
                    if (curr.id === action.payload.id) {
                        return { ...curr, ...action.payload };
                    }
                    return curr;
                })
            };
        case tourSettingsConstants.UPDATE_TS_CURRENCY_ERROR:
            return {
                travel_location_loading_curr: false,
                error: action.payload
            };

        default:
            return state;
    }
};
