import { currencyConstants } from 'store/constants/';

const initialState = {
    loading: false,
    error: '',
    inserted: false,
    remeoved: false,
    items: [],
    rows: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case currencyConstants.ADD_CURRENCY_REQUEST:
            return {
                ...state,
                loading: true
            };
        case currencyConstants.ADD_CURRENCY_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
                items: [...state.items, action.payload],
                rows: [...state.rows, action.payload]
            };
        case currencyConstants.ADD_CURRENCY_ERROR:
            return {
                loading: false,
                error: action.payload
            };
        case currencyConstants.GET_CURRENCY_REQUEST:
            return {
                ...state,
                loading: true
            };
        case currencyConstants.GET_CURRENCY_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
                inserted: true,
                items: action.payload,
                rows: action.payload.map((currency) => {
                    return { id: currency.id, col1: currency.name, col2: currency.symbol };
                })
            };
        case currencyConstants.GET_CURRENCY_ERROR:
            return {
                loading: false,
                error: action.payload
            };
        case currencyConstants.REMOVE_CURRENCY_REQUEST:
            return {
                ...state,
                loading: true
            };
        case currencyConstants.REMOVE_CURRENCY_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
                remeoved: true,
                items: state.items.filter((currency) => {
                    return currency.id !== action.payload;
                }),
                rows: state.rows.filter((currency) => {
                    return currency.id !== action.payload;
                })
            };
        case currencyConstants.REMOVE_CURRENCY_ERROR:
            return {
                loading: false,
                error: action.payload
            };
        case currencyConstants.UPDATE_CURRENCY_REQUEST:
            return {
                ...state,
                loading: true
            };
        case currencyConstants.UPDATE_CURRENCY_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
                remeoved: true,
                items: state.items.map((currency) => {
                    if (currency.id === action.payload.id) {
                        return { ...currency, name: action.payload.name, symbol: action.payload.symbol };
                    }
                    return currency;
                }),
                rows: state.rows.map((curr) => {
                    if (curr.id === action.payload.id) {
                        return { ...curr, ...action.payload };
                    }
                    return curr;
                })
            };
        case currencyConstants.UPDATE_CURRENCY_ERROR:
            return {
                loading: false,
                error: action.payload
            };

        default:
            return state;
    }
};
