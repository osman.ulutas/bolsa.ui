import { paymentTypeConstants } from 'store/constants/';

const initialState = {
    loading: false,
    error: '',
    inserted: false,
    remeoved: false,
    items: [],
    rows: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case paymentTypeConstants.ADD_PAYMENT_TYPE_REQUEST:
            return {
                ...state,
                loading: true
            };
        case paymentTypeConstants.ADD_PAYMENT_TYPE_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
                items: [...state.items, action.payload],
                rows: [...state.rows, action.payload]
            };
        case paymentTypeConstants.ADD_PAYMENT_TYPE_ERROR:
            return {
                loading: false,
                error: action.payload
            };
        case paymentTypeConstants.GET_PAYMENT_TYPE_REQUEST:
            return {
                ...state,
                loading: true
            };
        case paymentTypeConstants.GET_PAYMENT_TYPE_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
                inserted: true,
                items: action.payload,
                rows: action.payload.map((paymentType) => {
                    return { id: paymentType.id, col1: paymentType.name };
                })
            };
        case paymentTypeConstants.GET_PAYMENT_TYPE_ERROR:
            return {
                loading: false,
                error: action.payload
            };
        case paymentTypeConstants.REMOVE_PAYMENT_TYPE_REQUEST:
            return {
                ...state,
                loading: true
            };
        case paymentTypeConstants.REMOVE_PAYMENT_TYPE_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
                remeoved: true,
                items: state.items.filter((paymentType) => {
                    return paymentType.id !== action.payload;
                }),
                rows: state.rows.filter((paymentType) => {
                    return paymentType.id !== action.payload;
                })
            };
        case paymentTypeConstants.REMOVE_PAYMENT_TYPE_ERROR:
            return {
                loading: false,
                error: action.payload
            };
        case paymentTypeConstants.UPDATE_PAYMENT_TYPE_REQUEST:
            return {
                ...state,
                loading: true
            };
        case paymentTypeConstants.UPDATE_PAYMENT_TYPE_SUCCESS:
            return {
                ...state,
                loading: false,
                error: '',
                remeoved: true,
                items: state.items.map((paymentType) => {
                    if (paymentType.id === action.payload.id) {
                        return { ...paymentType, name: action.payload.name };
                    }
                    return paymentType;
                }),
                rows: state.rows.map((curr) => {
                    if (curr.id === action.payload.id) {
                        return { ...curr, ...action.payload };
                    }
                    return curr;
                })
            };
        case paymentTypeConstants.UPDATE_PAYMENT_TYPE_ERROR:
            return {
                loading: false,
                error: action.payload
            };

        default:
            return state;
    }
};
