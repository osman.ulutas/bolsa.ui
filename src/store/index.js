import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers';
import thunk from 'redux-thunk';

// ==============================|| REDUX - MAIN STORE ||============================== //

const store = createStore(rootReducer, applyMiddleware(thunk));
const persister = 'Free';

export { store, persister };
