import axios from 'axios';

import { authHeader } from './auth-header';

const AUTH_TOKEN = authHeader();

export const client = axios.create({
    baseURL: 'http://localhost:5032/api/'
});
