import React, { useState, useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import MainCard from 'ui-component/cards/MainCard';
import { Grid, Link } from '@mui/material';
import { DataGrid, GridToolbar, GridActionsCellItem, GridOverlay } from '@mui/x-data-grid';
import LinearProgress from '@mui/material/LinearProgress';
import MenuItem from '@mui/material/MenuItem';

import Stack from '@mui/material/Stack';
import DeleteIcon from '@mui/icons-material/Delete';

// project imports
import Box from '@mui/material/Box';

import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

import { useNavigate } from 'react-router-dom';

import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import EditIcon from '@mui/icons-material/Edit';

import Chip from '@mui/material/Chip';
import Autocomplete from '@mui/material/Autocomplete';

import CloseIcon from '@mui/icons-material/Close';
import { styled } from '@mui/material/styles';
import { client } from '_helpers/axios-settings';

import {
    getTravelLocation,
    addTravelLocation,
    deleteTravelLocation,
    updateTravelLocation,
    getTravelLocationItem,
    addTravelLocationItem,
    deleteTravelLocationItem,
    updateTravelLocationItem
} from 'store/actions/toursettings.actions';
import { getCountry, getCityByCountryId, getDistrictByCityId } from 'store/actions/country.actions';
import { boolean } from 'yup';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div role="tabpanel" hidden={value !== index} id={`simple-tabpanel-${index}`} aria-labelledby={`simple-tab-${index}`} {...other}>
            {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`
    };
}

function CustomLoadingOverlay() {
    return (
        <GridOverlay>
            <div style={{ position: 'absolute', top: 0, width: '100%' }}>
                <LinearProgress />
            </div>
        </GridOverlay>
    );
}

// =============================|| TABLER ICONS ||============================= //

const TourSettings = () => {
    const history = useNavigate();
    const dispatch = useDispatch();
    const toursettings = useSelector((state) => state.toursettings);
    const country = useSelector((state) => state.country);
    const payment = useSelector((state) => state.paymentType);
    const [value, setValue] = React.useState(0);

    // ==============================|| TRAVEL LOCATION ||============================== //

    const [roomTypeDialog, setTravelLocationOpen] = React.useState(false);
    const [name, setTravelLocName] = useState('');
    const [roomPageSize, setRoomPageSize] = useState(5);
    const [selectedTravelLocationRows, setSelectedTravelLocationRows] = React.useState([]);
    const [editTravelLocationRowsModel, setTravelLocationEditRowsModel] = React.useState({});
    const [citySelection, setCitySelection] = useState('');
    const [countrySelection, setCountrySelection] = useState('');
    const [districtSelection, setDistrictSelection] = useState('');
    const [addTravelLocationItemDialog, setAddTravelLocationItemDialog] = useState(false);
    const [selectedTags, setSelectedTags] = useState([]);
    const [travelLocId, setTravelLocId] = useState('');

    const getCityHandle = (id) => {
        setCountrySelection(id);
        dispatch(getCityByCountryId(id));
    };

    const getDistrictHandle = (id) => {
        setCitySelection(id);
        dispatch(getDistrictByCityId(id));
    };

    const handleTravelLocationClickOpen = () => {
        setTravelLocationOpen(true);
    };

    const handleTravelLocationClose = () => {
        setTravelLocationOpen(false);
    };

    const handleTravelLocationSave = () => {
        dispatch(addTravelLocation({ name, countryId: countrySelection, cityId: citySelection, districtionId: districtSelection }));
        handleTravelLocationClose();
    };

    const handleAddTravelLocationItemDialogClose = () => {
        setAddTravelLocationItemDialog(false);
    };

    const handleAddTravelLocationItemDialogOpen = (id) => {
        setAddTravelLocationItemDialog(true);
        setTravelLocId(id);
    };

    const onTravelLocationFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onTravelLocationPageChangeHander = (number) => {
        console.log(number);
    };

    const handleTravelLocationDeleteClick = (id) => {
        dispatch(deleteTravelLocation(id));
    };

    const handeTravelLocationCellEditCommit = (obj) => {
        if (!Object.prototype.hasOwnProperty.call(obj, 'cellMode')) {
            const comObj = {
                [obj.field]: obj.value
            };
            const newObj = toursettings.travel_location_rows.filter((room_type) => {
                return room_type.id === obj.id;
            });
            const retObj = { ...newObj[0], ...comObj };
            dispatch(updateTravelLocation(retObj));
        }
    };

    const handleAddTravelLocationItemSave = async () => {
        const { data } = await client.get(`bolsa/TravelLocation/travellocationitem/${travelLocId}`);
        setSelectedTags(data);
    };

    const handeSelectedTags = async (ev, values) => {
        setSelectedTags(values);
        const { data } = await client.put(`bolsa/TravelLocation/travellocationitem/${travelLocId}`);
    };

    const top100Films = [
        { title: 'The Shawshank Redemption', year: 1994 },
        { title: 'The Godfather', year: 1972 },
        { title: 'The Godfather: Part II', year: 1974 },
        { title: 'The Dark Knight', year: 2008 },
        { title: '12 Angry Men', year: 1957 },
        { title: "Schindler's List", year: 1993 },
        { title: 'Pulp Fiction', year: 1994 },
        {
            title: 'The Lord of the Rings: The Return of the King',
            year: 2003
        },
        { title: 'The Good, the Bad and the Ugly', year: 1966 },
        { title: 'Fight Club', year: 1999 },
        {
            title: 'The Lord of the Rings: The Fellowship of the Ring',
            year: 2001
        },
        {
            title: 'Star Wars: Episode V - The Empire Strikes Back',
            year: 1980
        },
        { title: 'Forrest Gump', year: 1994 },
        { title: 'Inception', year: 2010 },
        {
            title: 'The Lord of the Rings: The Two Towers',
            year: 2002
        },
        { title: "One Flew Over the Cuckoo's Nest", year: 1975 },
        { title: 'Goodfellas', year: 1990 },
        { title: 'The Matrix', year: 1999 },
        { title: 'Seven Samurai', year: 1954 },
        {
            title: 'Star Wars: Episode IV - A New Hope',
            year: 1977
        }
    ];
    const columnsTravelLocation = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', flex: 1, editable: true },
        { field: 'col2', headerName: 'Country', flex: 1 },
        { field: 'col3', headerName: 'City', flex: 1 },
        { field: 'col4', headerName: 'Distriction', flex: 1 },
        { field: 'col5', hide: true },
        { field: 'col6', hide: true },
        { field: 'col7', hide: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleTravelLocationDeleteClick(id)}
                        color="inherit"
                    />,
                    <GridActionsCellItem
                        icon={<EditIcon />}
                        label="Add or Remove"
                        onClick={() => handleAddTravelLocationItemDialogOpen(id)}
                        color="inherit"
                    />
                ];
            }
        }
    ];

    // ==============================|| TRAVEL LOCATION ITEM ||============================== //

    const [roomTypeItemDialog, setTravelLocItemOpen] = React.useState(false);
    const [locItemName, setTravelLocItemName] = useState('');
    const [travelLocITemPageSize, setTravelLocITemPageSize] = useState(5);
    const [selectedTravelLocItemRows, setSelectedTravelLocItemRows] = React.useState([]);
    const [editTravelLocItemRowsModel, setTravelLocItemEditRowsModel] = React.useState({});
    const [cityItemSelection, setCityItemSelection] = useState('');
    const [countryItemSelection, setCountryItemSelection] = useState('');
    const [optinalSelection, setOptionalTypeSelection] = useState('');
    const [districtItemSelection, setDistrictItemSelection] = useState('');
    const [paymentTypeSelection, setPaymentTypeSelection] = useState('');
    const [canSalePart, setCanSalePart] = useState(0);
    const [isOther, setIsOther] = useState(0);

    const optionalStatus = [
        {
            name: 'Optinal',
            id: 1
        },
        {
            name: 'Default',
            id: 0
        }
    ];

    const handleTravelLocItemClickOpen = (id) => {
        history({
            pathname: `/tours/edit/cancellationpolicydetail/${id}`
        });
    };

    const getCityItemHandle = (id) => {
        setCountryItemSelection(id);
        dispatch(getCityByCountryId(id));
    };

    const getDistrictItemHandle = (id) => {
        setCityItemSelection(id);
        dispatch(getDistrictByCityId(id));
    };

    const handleTravelLocItemClose = () => {
        setTravelLocItemOpen(false);
    };

    const handleTravelLocItemSave = () => {
        debugger;
        dispatch(
            addTravelLocationItem({
                name: locItemName,
                countryId: countryItemSelection,
                cityId: cityItemSelection,
                districtionId: districtItemSelection,
                canSalePart,
                isOther,
                optionalStatus: optinalSelection,
                paymentTypeId: paymentTypeSelection
            })
        );
        handleTravelLocItemClose();
    };

    const onTravelLocItemFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onTravelLocItemPageChangeHander = (number) => {
        console.log(number);
    };

    const handleTravelLocItemDeleteClick = (id) => {
        dispatch(deleteTravelLocationItem(id));
    };

    const handleLocItemEditClick = (id) => {
        history({
            pathname: `/tours/edit/${id}`
        });
    };

    const columnsTravelLocItem = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', flex: 1 },
        { field: 'col2', headerName: 'Country', flex: 1 },
        { field: 'col3', headerName: 'City', flex: 1 },
        { field: 'col4', headerName: 'Distriction', flex: 1 },
        { field: 'col5', hide: true },
        { field: 'col6', hide: true },
        { field: 'col7', hide: true },
        { field: 'col8', headerName: 'Can Sale Part', flex: 1, type: 'boolean' },
        { field: 'col9', headerName: 'İs Other', flex: 1, type: 'boolean' },
        { field: 'col10', hide: true },
        { field: 'col11', headerName: 'Payment Type', flex: 1 },
        { field: 'col12', headerName: 'Optinal Status', flex: 1 },
        { field: 'col13', hide: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleTravelLocItemDeleteClick(id)}
                        color="inherit"
                    />,
                    <GridActionsCellItem icon={<EditIcon />} label="Edit" onClick={() => handleLocItemEditClick(id)} color="inherit" />
                ];
            }
        }
    ];

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    useEffect(() => {
        dispatch(getTravelLocation());
        dispatch(getTravelLocationItem());
        dispatch(getCountry());
    }, []);

    return (
        <MainCard title="Tour Settings">
            <Box sx={{ width: '100%' }}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                        <Tab label="Travel Location" {...a11yProps(0)} />
                        <Tab label="Travel Location Item" {...a11yProps(1)} />
                    </Tabs>
                </Box>
                <TabPanel value={value} index={0}>
                    <Grid container spacing={2} alignItems="flexStart">
                        <Grid item xs={12} md={12}>
                            <Button variant="outlined" onClick={handleTravelLocationClickOpen}>
                                New
                            </Button>

                            <Dialog open={addTravelLocationItemDialog} onClose={handleAddTravelLocationItemDialogClose}>
                                <DialogTitle>Add New Travel Location Item</DialogTitle>
                                <DialogContent sx={{ display: 'flex', flexDirection: 'column', p: 3 }}>
                                    <DialogContentText sx={{ mb: 3 }}>
                                        To subscribe to this website, please enter your email address here. We will send updates
                                        occasionally.
                                    </DialogContentText>
                                    <Autocomplete
                                        multiple
                                        id="tags-standard"
                                        options={top100Films}
                                        getOptionLabel={(option) => option.title}
                                        onChange={(ev, values) => handeSelectedTags(values)}
                                        defaultValue={[top100Films[13]]}
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                variant="standard"
                                                label="Multiple values"
                                                placeholder="Location Items"
                                            />
                                        )}
                                    />
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleAddTravelLocationItemDialogClose}>Cancel</Button>
                                    <Button onClick={handleAddTravelLocationItemSave}>Save</Button>
                                </DialogActions>
                            </Dialog>
                            <Dialog open={roomTypeDialog} onClose={handleTravelLocationClose}>
                                <DialogTitle>Add New Travel Location </DialogTitle>
                                <DialogContent sx={{ display: 'flex', flexDirection: 'column' }}>
                                    <DialogContentText>
                                        To subscribe to this website, please enter your email address here. We will send updates
                                        occasionally.
                                    </DialogContentText>
                                    <TextField
                                        autoFocus
                                        onChange={(e) => setTravelLocName(e.target.value)}
                                        margin="dense"
                                        id="name"
                                        label="Name"
                                        type="text"
                                        fullWidth
                                        variant="standard"
                                    />
                                    <TextField
                                        id="country"
                                        select
                                        label="Country"
                                        value={countrySelection}
                                        onChange={(ev) => getCityHandle(ev.target.value)}
                                        helperText="Please select Country"
                                        variant="standard"
                                    >
                                        {country.country_items.map((option) => (
                                            <MenuItem key={option.id} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    <TextField
                                        id="city"
                                        select
                                        disabled={!country.city_fetched}
                                        label="City"
                                        value={citySelection}
                                        onChange={(ev) => getDistrictHandle(ev.target.value)}
                                        helperText="Please select city"
                                        variant="standard"
                                    >
                                        {country.city_by_country_items.map((option) => (
                                            <MenuItem key={option.id} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    <TextField
                                        id="district"
                                        select
                                        disabled={!country.district_fetched}
                                        label="District"
                                        value={districtSelection}
                                        onChange={(ev) => setDistrictSelection(ev.target.value)}
                                        helperText="Please select district"
                                        variant="standard"
                                    >
                                        {country.district_by_city_items.map((option) => (
                                            <MenuItem key={option.id} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleTravelLocationClose}>Cancel</Button>
                                    <Button onClick={handleTravelLocationSave}>Save</Button>
                                </DialogActions>
                            </Dialog>
                        </Grid>

                        <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                            <DataGrid
                                rows={toursettings.travel_location_rows || []}
                                components={{
                                    Toolbar: GridToolbar,
                                    LoadingOverlay: CustomLoadingOverlay
                                }}
                                loading={toursettings.travel_location_loading}
                                pageSize={roomPageSize}
                                onPageSizeChange={(newPageSize) => setRoomPageSize(newPageSize)}
                                rowsPerPageOptions={[5, 10, 20]}
                                onFilterModelChange={onTravelLocationFilterModelChangeHandler}
                                onPageChange={onTravelLocationPageChangeHander}
                                onCellEditCommit={handeTravelLocationCellEditCommit}
                                columns={columnsTravelLocation}
                                pagination
                            />
                        </Grid>
                    </Grid>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <Grid container spacing={2} alignItems="flexStart">
                        <Grid item xs={12} md={12}>
                            <Button variant="outlined" onClick={handleTravelLocItemClickOpen}>
                                New
                            </Button>
                            <Dialog open={roomTypeItemDialog} onClose={handleTravelLocItemClose}>
                                <DialogTitle> Add New Travel Location Item</DialogTitle>
                                <DialogContent sx={{ display: 'flex', flexDirection: 'column' }}>
                                    <DialogContentText>
                                        To subscribe to this website, please enter your email address here. We will send updates
                                        occasionally.
                                    </DialogContentText>
                                    <TextField
                                        autoFocus
                                        onChange={(e) => setTravelLocItemName(e.target.value)}
                                        margin="dense"
                                        id="name"
                                        label="Name"
                                        type="text"
                                        fullWidth
                                        variant="standard"
                                    />
                                    <TextField
                                        id="district"
                                        select
                                        label="Optional Status"
                                        value={optinalSelection}
                                        onChange={(ev) => setOptionalTypeSelection(ev.target.value)}
                                        helperText="Please select a optinal status"
                                        variant="standard"
                                    >
                                        {optionalStatus.map((option) => (
                                            <MenuItem key={option.id} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    <TextField
                                        id="district"
                                        select
                                        label="Payment Type"
                                        value={paymentTypeSelection}
                                        onChange={(ev) => setPaymentTypeSelection(ev.target.value)}
                                        helperText="Please select a payment type"
                                        variant="standard"
                                    >
                                        {payment.items.map((option) => (
                                            <MenuItem key={option.id} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    <TextField
                                        id="country"
                                        select
                                        label="Country"
                                        value={countryItemSelection}
                                        onChange={(ev) => getCityItemHandle(ev.target.value)}
                                        helperText="Please select Country"
                                        variant="standard"
                                    >
                                        {country.country_items.map((option) => (
                                            <MenuItem key={option.id} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    <TextField
                                        id="city"
                                        select
                                        disabled={!country.city_fetched}
                                        label="City"
                                        value={cityItemSelection}
                                        onChange={(ev) => getDistrictItemHandle(ev.target.value)}
                                        helperText="Please select city"
                                        variant="standard"
                                    >
                                        {country.city_by_country_items.map((option) => (
                                            <MenuItem key={option.id} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    <TextField
                                        id="district"
                                        select
                                        disabled={!country.district_fetched}
                                        label="District"
                                        value={districtItemSelection}
                                        onChange={(ev) => setDistrictItemSelection(ev.target.value)}
                                        helperText="Please select district"
                                        variant="standard"
                                    >
                                        {country.district_by_city_items.map((option) => (
                                            <MenuItem key={option.id} value={option.id}>
                                                {option.name}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                    <FormGroup>
                                        <FormControlLabel
                                            control={<Checkbox onChange={(ev) => setIsOther(ev.target.checked)} />}
                                            label="Is Other"
                                        />
                                        <FormControlLabel
                                            control={<Checkbox onChange={(ev) => setCanSalePart(ev.target.checked)} />}
                                            label="Can sale part"
                                        />
                                    </FormGroup>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleTravelLocItemClose}> Cancel </Button>

                                    <Button onClick={handleTravelLocItemSave}> Save </Button>
                                </DialogActions>
                            </Dialog>
                        </Grid>

                        <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                            <DataGrid
                                rows={toursettings.travel_location_item_rows || []}
                                components={{
                                    Toolbar: GridToolbar,
                                    LoadingOverlay: CustomLoadingOverlay
                                }}
                                loading={toursettings.travel_location_item_loading}
                                pageSize={travelLocITemPageSize}
                                onPageSizeChange={(newPageSize) => setTravelLocITemPageSize(newPageSize)}
                                rowsPerPageOptions={[5, 10, 20]}
                                onFilterModelChange={onTravelLocItemFilterModelChangeHandler}
                                onPageChange={onTravelLocItemPageChangeHander}
                                columns={columnsTravelLocItem}
                                pagination
                            />
                        </Grid>
                    </Grid>
                </TabPanel>
            </Box>
        </MainCard>
    );
};

export default TourSettings;
