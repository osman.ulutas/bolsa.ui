import { Grid, Link } from '@mui/material';
import MuiTypography from '@mui/material/Typography';

// project imports
import SubCard from 'ui-component/cards/SubCard';
import MainCard from 'ui-component/cards/MainCard';
import SecondaryAction from 'ui-component/cards/CardSecondaryAction';
import { customizeContants } from 'store/constants/customize.constants';

import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import Box from '@mui/material/Box';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';
import { useDispatch, useSelector } from 'react-redux';
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { getCountry, getCityByCountryId, getDistrictByCityId } from 'store/actions/country.actions';
import { addHotel } from 'store/actions/hotel.actions';

// ============================|| MATERIAL ICONS ||============================ //

const TourCreate = () => {
    const history = useNavigate();
    const dispatch = useDispatch();
    const hotelsettings = useSelector((state) => state.hotelsettings);
    const currency = useSelector((state) => state.currency);
    const country = useSelector((state) => state.country);
    const hotel = useSelector((state) => state.hotel);

    useEffect(() => {
        dispatch(getCountry());
    }, []);

    const [startSelection, setStartSelection] = React.useState(hotelsettings.star_rows[0] ? hotelsettings.star_rows[0].col1 : '');
    const [categorySelection, setCategorySelection] = useState(
        hotelsettings.category_policy_rows[0] ? hotelsettings.category_policy_rows[0].col1 : ''
    );
    const [citySelection, setCitySelection] = useState('');
    const [countrySelection, setCountrySelection] = useState('');
    const [districtSelection, setDistrictSelection] = useState('');
    const [currencySelection, setCurrencySelection] = React.useState(currency.rows[0] ? currency.rows[0].col1 : '');
    const [hotelName, setHotelName] = React.useState('');
    const [description, setDescription] = React.useState('');
    const [postalCode, setPostalCode] = React.useState('');
    const [address, setAddress] = React.useState('');
    const [mobilPhone, setMobilePhone] = React.useState('');
    const [phone, setPhone] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [webSite, setWebSite] = React.useState('');

    const getCityHandle = (id) => {
        setCountrySelection(id);
        console.log(id);
        dispatch(getCityByCountryId(id));
    };

    const getDistrictHandle = (id) => {
        setCitySelection(id);
        console.log(id);
        dispatch(getDistrictByCityId(id));
    };

    const saveHotel = () => {
        dispatch(
            addHotel({
                name: hotelName,
                description,
                postCode: postalCode,
                mobilPhone,
                phone,
                email,
                webSite,
                address,
                starRating: startSelection,
                currencyId: currencySelection,
                categoryId: categorySelection,
                countryId: countrySelection,
                cityId: citySelection,
                districtionId: districtSelection
            })
        );
    };

    return (
        <MainCard
            title="Hotel Create"
            secondary={
                <Button variant="outlined" onClick={saveHotel}>
                    Save
                </Button>
            }
        >
            <Grid container spacing={customizeContants.gridSpacing}>
                <Grid item xs={12} sm={6}>
                    <SubCard title="Basic Information">
                        <Box
                            sx={{
                                '& .MuiTextField-root': { m: 1, width: '25ch' }
                            }}
                        >
                            <div>
                                <TextField
                                    id="standard-basic"
                                    label="Hotel Name"
                                    onChange={(ev) => setHotelName(ev.target.value)}
                                    variant="standard"
                                />
                                <TextField
                                    id="outlined-select-currency"
                                    select
                                    label="Star"
                                    value={startSelection}
                                    helperText="Please select star"
                                    onChange={(ev) => setStartSelection(ev.target.value)}
                                    variant="standard"
                                >
                                    {hotelsettings.star_rows.map((option) => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.col1}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                <TextField
                                    id="outlined-select-currency"
                                    select
                                    label="Category"
                                    value={categorySelection}
                                    onChange={(ev) => setCategorySelection(ev.target.value)}
                                    helperText="Please select category"
                                    variant="standard"
                                >
                                    {hotelsettings.category_policy_rows.map((option) => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.col1}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                <TextField
                                    id="standard-basic"
                                    label="Description"
                                    onChange={(ev) => setDescription(ev.target.value)}
                                    variant="standard"
                                />
                            </div>
                        </Box>
                    </SubCard>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <SubCard title="Address Information">
                        <Box
                            sx={{
                                '& .MuiTextField-root': { m: 1, width: '25ch' }
                            }}
                        >
                            <div>
                                <TextField
                                    id="country"
                                    select
                                    label="Country"
                                    value={countrySelection}
                                    onChange={(ev) => getCityHandle(ev.target.value)}
                                    helperText="Please select Country"
                                    variant="standard"
                                >
                                    {country.country_items.map((option) => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                <TextField
                                    id="city"
                                    select
                                    disabled={!country.city_fetched}
                                    label="City"
                                    value={citySelection}
                                    onChange={(ev) => getDistrictHandle(ev.target.value)}
                                    helperText="Please select city"
                                    variant="standard"
                                >
                                    {country.city_by_country_items.map((option) => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                <TextField
                                    id="district"
                                    select
                                    disabled={!country.district_fetched}
                                    label="District"
                                    value={districtSelection}
                                    onChange={(ev) => setDistrictSelection(ev.target.value)}
                                    helperText="Please select district"
                                    variant="standard"
                                >
                                    {country.district_by_city_items.map((option) => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                                <TextField
                                    id="standard-basic"
                                    label="Postal Code"
                                    variant="standard"
                                    onChange={(ev) => setPostalCode(ev.target.value)}
                                />
                                <TextField
                                    id="standard-basic"
                                    label="Address"
                                    variant="standard"
                                    onChange={(ev) => setAddress(ev.target.value)}
                                />
                            </div>
                        </Box>
                    </SubCard>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <SubCard title="Contact Information">
                        <Box
                            sx={{
                                '& .MuiTextField-root': { m: 1, width: '25ch' }
                            }}
                        >
                            <div>
                                <TextField
                                    id="standard-basic"
                                    label="Mobile Phone"
                                    variant="standard"
                                    onChange={(ev) => setMobilePhone(ev.target.value)}
                                />
                                <TextField
                                    id="standard-basic"
                                    label="Phone"
                                    variant="standard"
                                    onChange={(ev) => setPhone(ev.target.value)}
                                />
                                <TextField
                                    id="standard-basic"
                                    label="Email"
                                    variant="standard"
                                    onChange={(ev) => setEmail(ev.target.value)}
                                />
                                <TextField
                                    id="standard-basic"
                                    label="Web Site"
                                    variant="standard"
                                    onChange={(ev) => setWebSite(ev.target.value)}
                                />
                            </div>
                        </Box>
                    </SubCard>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <SubCard title="Currency Information">
                        <Box
                            sx={{
                                '& .MuiTextField-root': { m: 1, width: '25ch' }
                            }}
                        >
                            <div>
                                <TextField
                                    id="outlined-select-currency"
                                    select
                                    label="Currency"
                                    value={currencySelection}
                                    onChange={(ev) => setCurrencySelection(ev.target.value)}
                                    helperText="Please select currency"
                                    variant="standard"
                                >
                                    {currency.rows.map((option) => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.col1}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </div>
                        </Box>
                    </SubCard>
                </Grid>
            </Grid>
        </MainCard>
    );
};

export default TourCreate;
