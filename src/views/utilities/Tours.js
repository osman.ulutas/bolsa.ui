import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import MainCard from 'ui-component/cards/MainCard';
import { Grid } from '@mui/material';
import { DataGrid, GridToolbar, GridActionsCellItem, GridOverlay } from '@mui/x-data-grid';
import LinearProgress from '@mui/material/LinearProgress';
import Stack from '@mui/material/Stack';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';

import { Link, useNavigate } from 'react-router-dom';
import { getOneHotel, getHotel, deleteHotel, updateHotel } from 'store/actions/hotel.actions';

// =============================|| TABLER ICONS ||============================= //

function CustomLoadingOverlay() {
    return (
        <GridOverlay>
            <div style={{ position: 'absolute', top: 0, width: '100%' }}>
                <LinearProgress />
            </div>
        </GridOverlay>
    );
}

const onPageChangeHander = (number) => {
    console.log(number);
};

const onFilterModelChangeHandler = (filter) => {
    console.log(filter);
};

const Tours = () => {
    const dispatch = useDispatch();
    const history = useNavigate();
    const [open, setOpen] = React.useState(false);
    const [pageSize, setPageSize] = useState(5);
    const [name, setName] = useState('');
    const [symbol, setSymbol] = useState('');
    const [selectedRows, setSelectedRows] = React.useState([]);
    const [rows, setRows] = React.useState([]);
    const [editRowsModel, setEditRowsModel] = React.useState({});

    const hotel = useSelector((state) => state.hotel);

    const handleDeleteClick = (id) => {
        dispatch(deleteHotel(id));
    };

    const handleEditClick = (id) => {
        history({
            pathname: `/hotels/edit/${id}`
        });
    };
    useEffect(() => {
        dispatch(getHotel());
    }, []);

    const handleClose = () => {
        setOpen(false);
    };

    const columns = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', width: 150, editable: true },
        { field: 'col2', headerName: 'Status', width: 150, editable: true },
        { field: 'col3', headerName: 'City', width: 150, editable: true },
        { field: 'col4', headerName: 'Phone', width: 150, editable: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem icon={<DeleteIcon />} label="Delete" onClick={() => handleDeleteClick(id)} color="inherit" />,
                    <GridActionsCellItem icon={<EditIcon />} label="Edit" onClick={() => handleEditClick(id)} color="inherit" />
                ];
            }
        }
    ];

    const handleEditRowsModelChange = (model) => {
        setEditRowsModel(model);
    };

    const handeCellEditCommit = (obj) => {
        if (!Object.prototype.hasOwnProperty.call(obj, 'cellMode')) {
            dispatch(updateHotel({ id: obj.id, name: obj.value }));
        }
    };

    const handleRowSelection = (ids) => {
        const selectedIDs = new Set(ids);
        const selectedRows = rows.filter((row) => selectedIDs.has(row.id));

        setSelectedRows(selectedRows);
    };

    return (
        <MainCard title="Hotel">
            <Grid container spacing={2} alignItems="flexStart">
                <Grid item xs={12} md={12}>
                    <Link to="/hotels/create">
                        <Button variant="outlined">New</Button>
                    </Link>
                </Grid>
                <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                    <DataGrid
                        rows={hotel.rows || []}
                        components={{
                            Toolbar: GridToolbar,
                            LoadingOverlay: CustomLoadingOverlay
                        }}
                        loading={hotel.loading}
                        pageSize={pageSize}
                        onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
                        rowsPerPageOptions={[5, 10, 20]}
                        onFilterModelChange={onFilterModelChangeHandler}
                        onPageChange={onPageChangeHander}
                        onSelectionModelChange={handleRowSelection}
                        onEditRowsModelChange={handleEditRowsModelChange}
                        onCellEditCommit={handeCellEditCommit}
                        columns={columns}
                        pagination
                    />
                </Grid>
            </Grid>
        </MainCard>
    );
};

export default Tours;
