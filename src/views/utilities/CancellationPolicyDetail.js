import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import Box from '@mui/material/Box';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import MainCard from 'ui-component/cards/MainCard';
import { Grid } from '@mui/material';
import { DataGrid, GridToolbar, GridActionsCellItem, GridOverlay } from '@mui/x-data-grid';
import LinearProgress from '@mui/material/LinearProgress';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { useParams } from 'react-router-dom';
import {
    getCancellationPolicyItem,
    addCancellationPolicyItem,
    deleteCancellationPolicyItem,
    updateCancellationPolicyItem
} from 'store/actions/hotelsettings.actions';

// ==============================|| CHILD POLICIY ||============================== //

function CustomLoadingOverlay() {
    return (
        <GridOverlay>
            <div style={{ position: 'absolute', top: 0, width: '100%' }}>
                <LinearProgress />
            </div>
        </GridOverlay>
    );
}

const CancellationPolicyDetail = () => {
    const params = useParams();
    const dispatch = useDispatch();
    const hotelsettings = useSelector((state) => state.hotelsettings);

    console.log(params.id);

    const [cancellationPolicyDialog, setCancellationPolicyOpen] = React.useState(false);
    const [day, setCancellatioDay] = useState('');
    const [refund, setRefund] = useState('');
    const [cancellationPageSize, setCancellationPolicyPageSize] = useState(5);
    const [selectedCancellationPolicyRows, setSelectedCancellationPolicyRows] = React.useState([]);
    const [editCancellationPolicyRowsModel, setCancellationPolicyEditRowsModel] = React.useState({});

    const handleCancellationPolicyClickOpen = () => {
        setCancellationPolicyOpen(true);
    };

    const handleCancellationPolicyClose = () => {
        setCancellationPolicyOpen(false);
    };

    const handleCancellationPolicySave = () => {
        dispatch(addCancellationPolicyItem(day, refund, params.id));
        handleCancellationPolicyClose();
    };

    const onCancellationPolicyFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onCancellationPolicyPageChangeHander = (number) => {
        console.log(number);
    };

    const handleCancellationPolicyDeleteClick = (id) => {
        dispatch(deleteCancellationPolicyItem(id));
    };

    const handeCancellationPolicyCellEditCommit = (obj) => {
        if (!Object.prototype.hasOwnProperty.call(obj, 'cellMode')) {
            const comObj = {
                [obj.field]: obj.value
            };
            const newObj = hotelsettings.cancellation_policy_item_rows.filter((cancellation_policy) => {
                return cancellation_policy.id === obj.id;
            });
            const retObj = { ...newObj[0], ...comObj };
            dispatch(updateCancellationPolicyItem(retObj));
        }
    };

    const columnsCancellationPolicy = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Day', flex: 1, editable: true },
        { field: 'col2', headerName: 'Refund', flex: 1, editable: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleCancellationPolicyDeleteClick(id)}
                        color="inherit"
                    />
                ];
            }
        }
    ];

    useEffect(() => {
        dispatch(getCancellationPolicyItem(params.id));
    }, []);

    return (
        <MainCard title="Cancellation Policy Detail">
            <Box sx={{ width: '100%' }}>
                <Grid container spacing={2} alignItems="flexStart">
                    <Grid item xs={12} md={12}>
                        <Button variant="outlined" onClick={handleCancellationPolicyClickOpen}>
                            New
                        </Button>
                        <Dialog open={cancellationPolicyDialog} onClose={handleCancellationPolicyClose}>
                            <DialogTitle>Add New Cancellation Policy Item</DialogTitle>
                            <DialogContent>
                                <DialogContentText>
                                    To subscribe to this website, please enter your email address here. We will send updates occasionally.
                                </DialogContentText>
                                <TextField
                                    autoFocus
                                    onChange={(e) => setCancellatioDay(e.target.value)}
                                    margin="dense"
                                    id="cancellationName"
                                    label="Name"
                                    type="number"
                                    fullWidth
                                    variant="standard"
                                />
                                <TextField
                                    margin="dense"
                                    onChange={(e) => setRefund(e.target.value)}
                                    id="refund"
                                    label="Refund"
                                    type="number"
                                    fullWidth
                                    variant="standard"
                                />
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleCancellationPolicyClose}>Cancel</Button>
                                <Button onClick={handleCancellationPolicySave}>Save</Button>
                            </DialogActions>
                        </Dialog>
                    </Grid>
                    <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                        <DataGrid
                            rows={hotelsettings.cancellation_policy_item_rows || []}
                            components={{
                                Toolbar: GridToolbar,
                                LoadingOverlay: CustomLoadingOverlay
                            }}
                            loading={hotelsettings.cancellation_policy_item_loading}
                            pageSize={cancellationPageSize}
                            onPageSizeChange={(newPageSize) => setCancellationPolicyPageSize(newPageSize)}
                            rowsPerPageOptions={[5, 10, 20]}
                            onFilterModelChange={onCancellationPolicyFilterModelChangeHandler}
                            onPageChange={onCancellationPolicyPageChangeHander}
                            onCellEditCommit={handeCancellationPolicyCellEditCommit}
                            columns={columnsCancellationPolicy}
                            pagination
                        />
                    </Grid>
                </Grid>
            </Box>
        </MainCard>
    );
};

export default CancellationPolicyDetail;
