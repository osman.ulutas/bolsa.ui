import React, { useState, useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import MainCard from 'ui-component/cards/MainCard';
import { Grid, Link } from '@mui/material';
import { DataGrid, GridToolbar, GridActionsCellItem, GridOverlay } from '@mui/x-data-grid';
import LinearProgress from '@mui/material/LinearProgress';
import Stack from '@mui/material/Stack';
import DeleteIcon from '@mui/icons-material/Delete';
import { addPaymentType, deletePaymentType, updatePaymentType } from 'store/actions/payment.actions';

// =============================||PAYMENT TYPES ||============================= //

const onPageChangeHander = (number) => {
    console.log(number);
};
const onChangeRowsPerPageHandleer = (number) => {
    console.log(number);
};
const onFilterModelChangeHandler = (filter) => {
    console.log(filter);
};

function CustomLoadingOverlay() {
    return (
        <GridOverlay>
            <div style={{ position: 'absolute', top: 0, width: '100%' }}>
                <LinearProgress />
            </div>
        </GridOverlay>
    );
}

const PaymentType = () => {
    const dispatch = useDispatch();
    const [open, setOpen] = React.useState(false);
    const [pageSize, setPageSize] = useState(5);
    const [name, setName] = useState('');
    const [selectedRows, setSelectedRows] = React.useState([]);
    const [rows, setRows] = React.useState([]);
    const [editRowsModel, setEditRowsModel] = React.useState({});

    const payment = useSelector((state) => state.paymentType);

    const handleDeleteClick = (id) => {
        dispatch(deletePaymentType(id));
    };

    useEffect(() => {}, []);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSave = async () => {
        dispatch(addPaymentType(name));
        handleClose();
    };

    const columns = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', width: 150, editable: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [<GridActionsCellItem icon={<DeleteIcon />} label="Delete" onClick={() => handleDeleteClick(id)} color="inherit" />];
            }
        }
    ];

    const handleEditRowsModelChange = (model) => {
        setEditRowsModel(model);
    };

    const handeCellEditCommit = (obj) => {
        if (!Object.prototype.hasOwnProperty.call(obj, 'cellMode')) {
            const comObj = {
                [obj.field]: obj.value
            };
            const newObj = payment.rows.filter((curr) => {
                return curr.id === obj.id;
            });
            const retObj = { ...newObj[0], ...comObj };
            dispatch(updatePaymentType(retObj));
        }
    };

    const handleRowSelection = (ids) => {
        const selectedIDs = new Set(ids);
        const selectedRows = rows.filter((row) => selectedIDs.has(row.id));

        setSelectedRows(selectedRows);
    };

    return (
        <MainCard title="Payment Type">
            <Grid container spacing={2} alignItems="flexStart">
                <Grid item xs={12} md={12}>
                    <Button variant="outlined" onClick={handleClickOpen}>
                        New
                    </Button>
                    <Dialog open={open} onClose={handleClose}>
                        <DialogTitle>Add new payment type</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                To subscribe to this website, please enter your email address here. We will send updates occasionally.
                            </DialogContentText>
                            <TextField
                                autoFocus
                                onChange={(e) => setName(e.target.value)}
                                margin="dense"
                                id="name"
                                label="Name"
                                type="text"
                                fullWidth
                                variant="standard"
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleClose}>Cancel</Button>
                            <Button onClick={handleSave}>Save</Button>
                        </DialogActions>
                    </Dialog>
                </Grid>
                <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                    <DataGrid
                        rows={payment.rows || []}
                        components={{
                            Toolbar: GridToolbar,
                            LoadingOverlay: CustomLoadingOverlay
                        }}
                        loading={payment.loading}
                        pageSize={pageSize}
                        onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
                        rowsPerPageOptions={[5, 10, 20]}
                        onFilterModelChange={onFilterModelChangeHandler}
                        onPageChange={onPageChangeHander}
                        onSelectionModelChange={handleRowSelection}
                        onEditRowsModelChange={handleEditRowsModelChange}
                        onCellEditCommit={handeCellEditCommit}
                        columns={columns}
                        pagination
                    />
                </Grid>
            </Grid>
        </MainCard>
    );
};

export default PaymentType;
