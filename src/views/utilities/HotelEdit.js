import { Grid, Link } from '@mui/material';

// project imports
import SubCard from 'ui-component/cards/SubCard';
import MainCard from 'ui-component/cards/MainCard';
import { customizeContants } from 'store/constants/customize.constants';

import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';

import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import {
    getOneHotel,
    updateHotel,
    addHotelRoom,
    addHotelChild,
    addHotelCancellation,
    deleteHotelChild,
    deleteHotelCancellation,
    deleteHotelRoom,
    addHotelContract,
    updateHotelContract,
    deleteHotelContract
} from 'store/actions/hotel.actions';
import { getCountry, getCityByCountryId, getDistrictByCityId } from 'store/actions/country.actions';

import { DataGrid, GridToolbar, GridActionsCellItem, GridOverlay } from '@mui/x-data-grid';
import LinearProgress from '@mui/material/LinearProgress';

import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import DeleteIcon from '@mui/icons-material/Delete';
import DetailsIcon from '@mui/icons-material/Details';
import CircularProgress from '@mui/material/CircularProgress';

import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import Stack from '@mui/material/Stack';
import { DatePicker, KeyboardDatePicker } from '@material-ui/pickers';

function CustomLoadingOverlay() {
    return (
        <GridOverlay>
            <div style={{ position: 'absolute', top: 0, width: '100%' }}>
                <LinearProgress />
            </div>
        </GridOverlay>
    );
}
function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div role="tabpanel" hidden={value !== index} id={`simple-tabpanel-${index}`} aria-labelledby={`simple-tab-${index}`} {...other}>
            {value === index && <Box sx={{ p: 3, display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between' }}>{children}</Box>}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`
    };
}

// ============================|| MATERIAL ICONS ||============================ //

const HotelEdit = () => {
    const params = useParams();
    const history = useNavigate();

    const dispatch = useDispatch();
    const hotelsettings = useSelector((state) => state.hotelsettings);
    const currency = useSelector((state) => state.currency);
    const country = useSelector((state) => state.country);
    const hotel = useSelector((state) => state.hotel);

    const [value, setValue] = React.useState(0);
    const [startSelection, setStartSelection] = React.useState('');
    const [categorySelection, setCategorySelection] = useState('');
    const [citySelection, setCitySelection] = useState('');
    const [countrySelection, setCountrySelection] = useState('');
    const [districtSelection, setDistrictSelection] = useState('');
    const [currencySelection, setCurrencySelection] = React.useState('');
    const [hotelName, setHotelName] = React.useState('');
    const [description, setDescription] = React.useState('');
    const [postalCode, setPostalCode] = React.useState('');
    const [address, setAddress] = React.useState('');
    const [mobilPhone, setMobilePhone] = React.useState('');
    const [phone, setPhone] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [webSite, setWebSite] = React.useState('');

    const getCityHandle = (id) => {
        dispatch(getCityByCountryId(id));
        setCountrySelection(id);
    };

    const getDistrictHandle = (id) => {
        dispatch(getDistrictByCityId(id));
        setCitySelection(id);
    };

    useEffect(() => {
        if (hotel.updated) {
            history({
                pathname: '/hotels/list'
            });
        }
    }, [hotel]);

    useEffect(() => {
        dispatch(getOneHotel(params.id));
        dispatch(getCountry());
    }, []);

    useEffect(() => {
        if (hotel.oneItemFetched) {
            setHotelName(hotel.oneItem.name);
            setDescription(hotel.oneItem.description);
            setPostalCode(hotel.oneItem.postCode);
            setAddress(hotel.oneItem.address);
            setMobilePhone(hotel.oneItem.mobilPhone);
            setPhone(hotel.oneItem.phone);
            setEmail(hotel.oneItem.email);
            setWebSite(hotel.oneItem.webSite);
            setStartSelection(hotel.oneItem.starRatingId);
            setCategorySelection(hotel.oneItem.categoryId);
            setCurrencySelection(hotel.oneItem.currencyId);
            getCityHandle(hotel.oneItem.countryId);
        }
    }, [hotel.oneItemFetched]);

    useEffect(() => {
        if (hotel.oneItemFetched && country.country_fetched && country.city_fetched && country.district_start_fetched) {
            getDistrictHandle(hotel.oneItem.cityId);
            setDistrictSelection(hotel.oneItem.districtionId);
        }
    }, [hotel, country]);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    // ==============================|| ROOM TYPE ||============================== //

    const [roomTypeDialog, setRoomTypeOpen] = React.useState(false);
    const [name, setName] = useState('');
    const [child, setChild] = useState('');
    const [roomTypeSelection, setRoomTypeSelection] = useState('');
    const [adult, setAdult] = useState('');
    const [roomPageSize, setRoomPageSize] = useState(5);
    const [selectedRoomTypeRows, setSelectedRoomTypeRows] = React.useState([]);
    const [editRoomTypeRowsModel, setRoomTypeEditRowsModel] = React.useState({});

    const handleRoomTypeClickOpen = () => {
        setRoomTypeOpen(true);
    };

    const handleRoomTypeClose = () => {
        setRoomTypeOpen(false);
    };

    const handleRoomTypeSave = () => {
        dispatch(addHotelRoom({ hotelId: params.id, roomTypeId: roomTypeSelection }));
        handleRoomTypeClose();
    };

    const onRoomTypeFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onRoomTypePageChangeHander = (number) => {
        console.log(number);
    };
    const addRoomType = () => {};

    const handleRoomTypeDeleteClick = (id) => {
        dispatch(deleteHotelRoom(id));
    };

    const columnsRoomType = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', flex: 1 },
        { field: 'col2', headerName: 'Child', flex: 1 },
        { field: 'col3', headerName: 'Adult', flex: 1 },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleRoomTypeDeleteClick(id)}
                        color="inherit"
                    />
                ];
            }
        }
    ];

    // ==============================|| CANCEELLATION POLICIY ||============================== //

    const [cancellationDialog, setCancellationOpen] = React.useState(false);
    const [cancellationName, setCancellationName] = useState('');
    const [cancellationSelection, setCancellationSelection] = useState('');
    const [cancellationPageSize, setCancellationPageSize] = useState(5);
    const [selectedCancellationRows, setSelectedCancellationRows] = React.useState([]);
    const [editCancellationRowsModel, setCancellationEditRowsModel] = React.useState({});

    const handleCancellationClickOpen = () => {
        setCancellationOpen(true);
    };

    const handleCancellationClose = () => {
        setCancellationOpen(false);
    };

    const handleCancellationSave = () => {
        dispatch(addHotelCancellation({ hotelId: params.id, cancellationPolicyId: cancellationSelection }));
        handleCancellationClose();
    };

    const onCancellationFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onCancellationPageChangeHander = (number) => {
        console.log(number);
    };

    const addCancellationType = () => {
        console.log();
    };

    const handleCancellationDeleteClick = (id) => {
        dispatch(deleteHotelCancellation(id));
    };

    const columnsCancellation = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', flex: 1 },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleCancellationDeleteClick(id)}
                        color="inherit"
                    />
                ];
            }
        }
    ];

    // ==============================|| CHILD POLICIY ||============================== //

    const [childPolicyDialog, setChildPolicyOpen] = React.useState(false);
    const [childPolicyName, setChildPolicyName] = useState('');
    const [childPolicySelection, setChildPolicySelection] = useState('');

    const [childPolicyPageSize, setChildPolicyPageSize] = useState(5);
    const [selectedChildPolicyRows, setSelectedChildPolicyRows] = React.useState([]);
    const [editChildPolicyRowsModel, setChildPolicyEditRowsModel] = React.useState({});

    const handleChildPolicyClickOpen = () => {
        setChildPolicyOpen(true);
    };

    const handleChildPolicyClose = () => {
        setChildPolicyOpen(false);
    };
    const handleChildPolicySave = () => {
        dispatch(addHotelChild({ hotelId: params.id, childPolicyId: childPolicySelection }));
        handleChildPolicyClose();
    };
    const handeUpdateHotelInforation = () => {
        dispatch(
            updateHotel({
                id: params.id,
                name: hotelName,
                description,
                postCode: postalCode,
                phone,
                mobilPhone,
                email,
                webSite,
                address,
                categoryId: categorySelection,
                starRatingId: startSelection,
                currencyId: currencySelection,
                countryId: countrySelection,
                cityId: citySelection,
                districtionId: districtSelection
            })
        );
        handleChildPolicyClose();
    };

    const onChildPolicyFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onChildPolicyPageChangeHander = (number) => {
        console.log(number);
    };
    const addChildPolicyType = () => {};

    const handleChildPolicyDeleteClick = (id) => {
        dispatch(deleteHotelChild(id));
    };

    const columnsChildPolicy = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', flex: 1 },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleChildPolicyDeleteClick(id)}
                        color="inherit"
                    />
                ];
            }
        }
    ];

    // ==============================|| CONTRACT POLICIY ||============================== //

    const [hotelContractDialog, setHotelContractOpen] = React.useState(false);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [priceList, setPriceList] = useState([]);
    const priceListArray = [];
    const [hotelContractPageSize, setHotelContractPageSize] = useState(5);
    const [selectedHotelContractRows, setSelectedHotelContractRows] = React.useState([]);
    const [editHotelContractRowsModel, setHotelContractEditRowsModel] = React.useState({});

    const handeChangePrice = (e) => {
        const { name, value } = e.target;
        setPriceList((prevState) => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleHotelContractClickOpen = () => {
        setHotelContractOpen(true);
    };

    const handleHotelContractClose = () => {
        setHotelContractOpen(false);
    };
    const handleHotelContractSave = () => {
        handleHotelContractClose();

        Object.keys(priceList).forEach((key) => {
            priceListArray.push({
                roomTypeId: key,
                price: priceList[key]
            });
        });
        const obj = {
            hotelId: params.id,
            startDate,
            endDate,
            hotelRoomPrice: priceListArray
        };

        dispatch(addHotelContract(obj));
    };

    const onHotelContractFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onHotelContractPageChangeHander = (number) => {
        console.log(number);
    };

    const handleHotelContractDeleteClick = (id) => {
        dispatch(deleteHotelContract(id));
    };

    const handeContractCellEditCommit = (obj) => {
        if (!Object.prototype.hasOwnProperty.call(obj, 'cellMode')) {
            let actId = 0;
            const comObj = {
                [obj.field]: obj.value
            };

            const newObj = hotel.contract_rows.filter((curr) => {
                return curr.id === obj.id;
            });

            if (obj.field === 'col3') {
                actId = newObj[0].col6;
            } else if (obj.field === 'col4') {
                actId = newObj[0].col7;
            } else if (obj.field === 'col5') {
                actId = newObj[0].col8;
            }

            const retObj = { ...newObj[0], ...comObj };
            dispatch(updateHotelContract(retObj, actId, obj.value));
        }
    };

    const columnsHotelContract = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Start Date', type: 'date', flex: 1 },
        { field: 'col2', headerName: 'End Date', type: 'date', flex: 1 },
        { field: 'col3', headerName: 'Single', flex: 1, editable: true },
        { field: 'col4', headerName: 'Double', flex: 1, editable: true },
        { field: 'col5', headerName: 'Triple', flex: 1, editable: true },
        { field: 'col6', headerName: 'singleId', flex: 1, hide: true },
        { field: 'col7', headerName: 'doubleId', flex: 1, hide: true },
        { field: 'col8', headerName: 'tripleId', flex: 1, hide: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleHotelContractDeleteClick(id)}
                        color="inherit"
                    />
                ];
            }
        }
    ];

    return (
        <>
            {!hotel.oneItemFetched ? (
                <MainCard>
                    <Grid container spacing={customizeContants.gridSpacing}>
                        <Box sx={{ width: '100%', height: '400px', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <CircularProgress />
                        </Box>
                    </Grid>
                </MainCard>
            ) : (
                <MainCard
                    title="Hotel Edit"
                    secondary={
                        <Button onClick={handeUpdateHotelInforation} variant="outlined">
                            Update
                        </Button>
                    }
                >
                    <Grid container spacing={customizeContants.gridSpacing}>
                        <Box sx={{ width: '100%' }}>
                            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                                    <Tab label="Hotel Information" {...a11yProps(0)} />
                                    <Tab label="Rooms" {...a11yProps(1)} />
                                    <Tab label="Cancellation Policy" {...a11yProps(2)} />
                                    <Tab label="Child Policy" {...a11yProps(3)} />
                                    <Tab label="Manage Hotel Contract" {...a11yProps(4)} />
                                </Tabs>
                            </Box>
                            <TabPanel value={value} index={0}>
                                <Grid item xs={12} sm={6} sx={{ pr: 4, pb: 4 }}>
                                    <SubCard title="Basic Information">
                                        <Box
                                            sx={{
                                                '& .MuiTextField-root': { m: 1, width: '25ch' }
                                            }}
                                        >
                                            <div>
                                                <TextField
                                                    id="standard-basic"
                                                    label="Hotel Name"
                                                    value={hotelName}
                                                    onChange={(ev) => setHotelName(ev.target.value)}
                                                    variant="standard"
                                                />
                                                <TextField
                                                    id="outlined-select-currency"
                                                    select
                                                    label="Star"
                                                    value={startSelection}
                                                    helperText="Please select star"
                                                    onChange={(ev) => setStartSelection(ev.target.value)}
                                                    variant="standard"
                                                >
                                                    {hotelsettings.star_rows.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.col1}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                                <TextField
                                                    id="outlined-select-currency"
                                                    select
                                                    label="Category"
                                                    value={categorySelection}
                                                    onChange={(ev) => setCategorySelection(ev.target.value)}
                                                    helperText="Please select category"
                                                    variant="standard"
                                                >
                                                    {hotelsettings.category_policy_rows.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.col1}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                                <TextField
                                                    id="standard-basic"
                                                    label="Description"
                                                    value={description}
                                                    onChange={(ev) => setDescription(ev.target.value)}
                                                    variant="standard"
                                                />
                                            </div>
                                        </Box>
                                    </SubCard>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <SubCard title="Address Information">
                                        <Box
                                            sx={{
                                                '& .MuiTextField-root': { m: 1, width: '25ch' }
                                            }}
                                        >
                                            <div>
                                                <TextField
                                                    id="country"
                                                    select
                                                    label="Country"
                                                    value={countrySelection}
                                                    onChange={(ev) => getCityHandle(ev.target.value)}
                                                    helperText="Please select Country"
                                                    variant="standard"
                                                >
                                                    {country.country_items.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                                <TextField
                                                    id="city"
                                                    select
                                                    disabled={!country.city_fetched}
                                                    label="City"
                                                    value={citySelection}
                                                    onChange={(ev) => getDistrictHandle(ev.target.value)}
                                                    helperText="Please select city"
                                                    variant="standard"
                                                >
                                                    {country.city_by_country_items.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                                <TextField
                                                    id="district"
                                                    select
                                                    disabled={!country.district_fetched}
                                                    label="District"
                                                    value={districtSelection}
                                                    onChange={(ev) => setDistrictSelection(ev.target.value)}
                                                    helperText="Please select district"
                                                    variant="standard"
                                                >
                                                    {country.district_by_city_items.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                                <TextField
                                                    id="standard-basic"
                                                    label="Postal Code"
                                                    variant="standard"
                                                    value={postalCode}
                                                    onChange={(ev) => setPostalCode(ev.target.value)}
                                                />
                                                <TextField
                                                    id="standard-basic"
                                                    label="Address"
                                                    variant="standard"
                                                    value={address}
                                                    onChange={(ev) => setAddress(ev.target.value)}
                                                />
                                            </div>
                                        </Box>
                                    </SubCard>
                                </Grid>
                                <Grid item xs={12} sm={6} sx={{ pr: 4 }}>
                                    <SubCard title="Contact Information">
                                        <Box
                                            sx={{
                                                '& .MuiTextField-root': { m: 1, width: '25ch' }
                                            }}
                                        >
                                            <div>
                                                <TextField
                                                    id="standard-basic"
                                                    label="Mobile Phone"
                                                    variant="standard"
                                                    value={mobilPhone}
                                                    onChange={(ev) => setMobilePhone(ev.target.value)}
                                                />
                                                <TextField
                                                    id="standard-basic"
                                                    label="Phone"
                                                    variant="standard"
                                                    value={phone}
                                                    onChange={(ev) => setPhone(ev.target.value)}
                                                />
                                                <TextField
                                                    id="standard-basic"
                                                    label="Email"
                                                    value={email}
                                                    variant="standard"
                                                    onChange={(ev) => setEmail(ev.target.value)}
                                                />
                                                <TextField
                                                    id="standard-basic"
                                                    label="Web Site"
                                                    variant="standard"
                                                    value={webSite}
                                                    onChange={(ev) => setWebSite(ev.target.value)}
                                                />
                                            </div>
                                        </Box>
                                    </SubCard>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <SubCard title="Currency Information">
                                        <Box
                                            sx={{
                                                '& .MuiTextField-root': { m: 1, width: '25ch' }
                                            }}
                                        >
                                            <div>
                                                <TextField
                                                    id="outlined-select-currency"
                                                    select
                                                    label="Currency"
                                                    value={currencySelection}
                                                    onChange={(ev) => setCurrencySelection(ev.target.value)}
                                                    helperText="Please select currency"
                                                    variant="standard"
                                                >
                                                    {currency.rows.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.col1}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </div>
                                        </Box>
                                    </SubCard>
                                </Grid>
                            </TabPanel>
                            <TabPanel value={value} index={1}>
                                <Grid container spacing={2} alignItems="flexStart">
                                    <Grid item xs={12} md={12}>
                                        <Button variant="outlined" onClick={handleRoomTypeClickOpen}>
                                            Add
                                        </Button>
                                        <Dialog open={roomTypeDialog} onClose={handleRoomTypeClose}>
                                            <DialogTitle>Add Room Type</DialogTitle>
                                            <DialogContent>
                                                <DialogContentText>
                                                    To subscribe to this website, please enter your email address here. We will send updates
                                                    occasionally.
                                                </DialogContentText>
                                                <TextField
                                                    id="outlined-select-currency"
                                                    select
                                                    label="Room Type"
                                                    value={roomTypeSelection}
                                                    helperText="Please select a room type"
                                                    onChange={(ev) => setRoomTypeSelection(ev.target.value)}
                                                    variant="standard"
                                                >
                                                    {hotelsettings.room_type_rows.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.col1}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={handleRoomTypeClose}>Cancel</Button>
                                                <Button onClick={handleRoomTypeSave}>Save</Button>
                                            </DialogActions>
                                        </Dialog>
                                    </Grid>
                                    <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                                        <DataGrid
                                            rows={hotel.room_type_rows || []}
                                            components={{
                                                Toolbar: GridToolbar,
                                                LoadingOverlay: CustomLoadingOverlay
                                            }}
                                            loading={hotel.loading}
                                            pageSize={roomPageSize}
                                            onPageSizeChange={(newPageSize) => setRoomPageSize(newPageSize)}
                                            rowsPerPageOptions={[5, 10, 20]}
                                            onFilterModelChange={onRoomTypeFilterModelChangeHandler}
                                            onPageChange={onRoomTypePageChangeHander}
                                            columns={columnsRoomType}
                                            pagination
                                        />
                                    </Grid>
                                </Grid>
                            </TabPanel>
                            <TabPanel value={value} index={2}>
                                <Grid container spacing={2} alignItems="flexStart">
                                    <Grid item xs={12} md={12}>
                                        <Button variant="outlined" onClick={handleCancellationClickOpen}>
                                            New
                                        </Button>
                                        <Dialog open={cancellationDialog} onClose={handleCancellationClose}>
                                            <DialogTitle>Add Cancellation Policy</DialogTitle>
                                            <DialogContent>
                                                <DialogContentText>
                                                    To subscribe to this website, please enter your email address here. We will send updates
                                                    occasionally.
                                                </DialogContentText>
                                                <TextField
                                                    id="outlined-select-currency"
                                                    select
                                                    label="Cancellation Type"
                                                    value={cancellationSelection}
                                                    helperText="Please select a cancellation type"
                                                    onChange={(ev) => setCancellationSelection(ev.target.value)}
                                                    variant="standard"
                                                >
                                                    {hotelsettings.cancellation_policy_rows.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.col1}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={handleCancellationClose}>Cancel</Button>
                                                <Button onClick={handleCancellationSave}>Save</Button>
                                            </DialogActions>
                                        </Dialog>
                                    </Grid>
                                    <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                                        <DataGrid
                                            rows={hotel.cancellation_policy_rows || []}
                                            components={{
                                                Toolbar: GridToolbar,
                                                LoadingOverlay: CustomLoadingOverlay
                                            }}
                                            loading={hotel.loading}
                                            pageSize={cancellationPageSize}
                                            onPageSizeChange={(newPageSize) => setCancellationPageSize(newPageSize)}
                                            rowsPerPageOptions={[5, 10, 20]}
                                            onFilterModelChange={onCancellationFilterModelChangeHandler}
                                            onPageChange={onCancellationPageChangeHander}
                                            columns={columnsCancellation}
                                            pagination
                                        />
                                    </Grid>
                                </Grid>
                            </TabPanel>
                            <TabPanel value={value} index={3}>
                                <Grid container spacing={2} alignItems="flexStart">
                                    <Grid item xs={12} md={12}>
                                        <Button variant="outlined" onClick={handleChildPolicyClickOpen}>
                                            New
                                        </Button>
                                        <Dialog open={childPolicyDialog} onClose={handleChildPolicyClose}>
                                            <DialogTitle>Add Child Policy</DialogTitle>
                                            <DialogContent>
                                                <DialogContentText>
                                                    To subscribe to this website, please enter your email address here. We will send updates
                                                    occasionally.
                                                </DialogContentText>
                                                <TextField
                                                    id="outlined-select-currency"
                                                    select
                                                    label="Child Policy"
                                                    value={childPolicySelection}
                                                    helperText="Please select a child policy"
                                                    onChange={(ev) => setChildPolicySelection(ev.target.value)}
                                                    variant="standard"
                                                >
                                                    {hotelsettings.child_policy_rows.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.col1}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={handleChildPolicyClose}>Cancel</Button>
                                                <Button onClick={handleChildPolicySave}>Save</Button>
                                            </DialogActions>
                                        </Dialog>
                                    </Grid>
                                    <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                                        <DataGrid
                                            rows={hotel.child_policy_rows || []}
                                            components={{
                                                Toolbar: GridToolbar,
                                                LoadingOverlay: CustomLoadingOverlay
                                            }}
                                            loading={hotel.loading}
                                            pageSize={childPolicyPageSize}
                                            onPageSizeChange={(newPageSize) => setChildPolicyPageSize(newPageSize)}
                                            rowsPerPageOptions={[5, 10, 20]}
                                            onFilterModelChange={onChildPolicyFilterModelChangeHandler}
                                            onPageChange={onChildPolicyPageChangeHander}
                                            columns={columnsChildPolicy}
                                            pagination
                                        />
                                    </Grid>
                                </Grid>
                            </TabPanel>
                            <TabPanel value={value} index={4}>
                                <Grid container spacing={2} alignItems="flexStart">
                                    <Grid item xs={12} md={12}>
                                        <Button variant="outlined" onClick={handleHotelContractClickOpen}>
                                            New
                                        </Button>
                                        <Dialog open={hotelContractDialog} onClose={handleHotelContractClose}>
                                            <DialogTitle>Add Hotel Contract</DialogTitle>
                                            <DialogContent>
                                                <DialogContentText sx={{ mb: 3 }}>
                                                    To subscribe to this website, please enter your email address here. We will send updates
                                                    occasionally.
                                                </DialogContentText>
                                                <LocalizationProvider dateAdapter={AdapterDateFns}>
                                                    <Stack spacing={3}>
                                                        <DesktopDatePicker
                                                            label="Start Date"
                                                            onChange={setStartDate}
                                                            format="yyyy/MM/dd"
                                                            value={startDate}
                                                            renderInput={(params) => <TextField {...params} />}
                                                        />
                                                        <DesktopDatePicker
                                                            label="End Date"
                                                            onChange={setEndDate}
                                                            value={endDate}
                                                            format="yyyy/MM/dd"
                                                            renderInput={(params) => <TextField {...params} />}
                                                        />

                                                        {hotel.room_type_rows.map((option) => (
                                                            <Box sx={{ display: 'flex' }}>
                                                                <MenuItem key={option.id} value={option.id}>
                                                                    {option.col1}
                                                                </MenuItem>
                                                                <TextField
                                                                    id="standard-basic"
                                                                    name={option.id}
                                                                    label={`${option.col1} Price`}
                                                                    onChange={handeChangePrice}
                                                                    variant="standard"
                                                                />
                                                            </Box>
                                                        ))}
                                                    </Stack>
                                                </LocalizationProvider>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={handleHotelContractClose}>Cancel</Button>
                                                <Button onClick={handleHotelContractSave}>Save</Button>
                                            </DialogActions>
                                        </Dialog>
                                    </Grid>
                                    <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                                        <DataGrid
                                            rows={hotel.contract_rows || []}
                                            components={{
                                                Toolbar: GridToolbar,
                                                LoadingOverlay: CustomLoadingOverlay
                                            }}
                                            loading={hotel.loading}
                                            pageSize={hotelContractPageSize}
                                            onPageSizeChange={(newPageSize) => setHotelContractPageSize(newPageSize)}
                                            rowsPerPageOptions={[5, 10, 20]}
                                            onFilterModelChange={onHotelContractFilterModelChangeHandler}
                                            onPageChange={onHotelContractPageChangeHander}
                                            columns={columnsHotelContract}
                                            onCellEditCommit={handeContractCellEditCommit}
                                            pagination
                                        />
                                    </Grid>
                                </Grid>
                            </TabPanel>
                        </Box>
                    </Grid>
                </MainCard>
            )}
        </>
    );
};

export default HotelEdit;
