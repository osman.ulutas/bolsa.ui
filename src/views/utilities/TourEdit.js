import { Grid, Link } from '@mui/material';

// project imports
import SubCard from 'ui-component/cards/SubCard';
import MainCard from 'ui-component/cards/MainCard';
import { customizeContants } from 'store/constants/customize.constants';

import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';

import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { getCityByCountryId, getDistrictByCityId } from 'store/actions/country.actions';

import { DataGrid, GridToolbar, GridActionsCellItem, GridOverlay } from '@mui/x-data-grid';
import LinearProgress from '@mui/material/LinearProgress';

import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import DeleteIcon from '@mui/icons-material/Delete';
import DetailsIcon from '@mui/icons-material/Details';
import CircularProgress from '@mui/material/CircularProgress';

import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import EditIcon from '@mui/icons-material/Edit';
import { client } from '_helpers/axios-settings';
import {
    updateTravelLocationItem,
    updateTravelLocationItemCurrency,
    getTravelLocationItemCurrency,
    deleteTravelLocationItemCurrency,
    addTravelLocationCurrency
} from 'store/actions/toursettings.actions';

function CustomLoadingOverlay() {
    return (
        <GridOverlay>
            <div style={{ position: 'absolute', top: 0, width: '100%' }}>
                <LinearProgress />
            </div>
        </GridOverlay>
    );
}
function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div role="tabpanel" hidden={value !== index} id={`simple-tabpanel-${index}`} aria-labelledby={`simple-tab-${index}`} {...other}>
            {value === index && <Box sx={{ p: 3, display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between' }}>{children}</Box>}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`
    };
}

// ============================|| MATERIAL ICONS ||============================ //

const TourEdit = () => {
    const params = useParams();
    const history = useNavigate();

    const dispatch = useDispatch();
    const currency = useSelector((state) => state.currency);
    const country = useSelector((state) => state.country);
    const payment = useSelector((state) => state.paymentType);
    const toursettings = useSelector((state) => state.toursettings);

    const [value, setValue] = React.useState(0);
    const [itemFetched, setItemFetched] = React.useState(false);
    const [locItemName, setTravelLocItemName] = useState('');
    const [cityItemSelection, setCityItemSelection] = useState('');
    const [countryItemSelection, setCountryItemSelection] = useState('');
    const [optinalSelection, setOptionalTypeSelection] = useState('');
    const [districtItemSelection, setDistrictItemSelection] = useState('');
    const [paymentTypeSelection, setPaymentTypeSelection] = useState('');
    const [canSalePart, setCanSalePart] = useState(false);
    const [isOther, setIsOther] = useState(false);

    const [roomPageSize, setRoomPageSize] = useState(5);
    const [selectedTravelLocationRows, setSelectedTravelLocationRows] = React.useState([]);
    const [editTravelLocationRowsModel, setTravelLocationEditRowsModel] = React.useState({});

    const [roomTypeItemDialog, setTravelLocationOpen] = React.useState(false);
    const [cost, setCost] = React.useState(0);
    const [price, setPrice] = React.useState(0);
    const [currencySelection, setCurrencySelection] = React.useState(0);

    const handeUpdateTravelLocItem = () => {
        dispatch(
            updateTravelLocationItem({
                id: params.id,
                name: locItemName,
                canSalePart,
                isOther,
                optionalStatus: optinalSelection,
                paymentTypeId: paymentTypeSelection,
                countryId: countryItemSelection,
                cityId: cityItemSelection,
                districtionId: districtItemSelection
            })
        );
    };

    const optionalStatus = [
        {
            name: 'Optinal',
            id: 1
        },
        {
            name: 'Default',
            id: 0
        }
    ];

    const getCityHandle = (id, select) => {
        if (select) {
            setCityItemSelection('');
            setDistrictItemSelection('');
        }
        dispatch(getCityByCountryId(id));
        setCountryItemSelection(id);
    };

    const getDistrictHandle = (id, select) => {
        if (select) {
            setDistrictItemSelection('');
        }
        dispatch(getDistrictByCityId(id));
        setCityItemSelection(id);
    };
    const fetchData = async () => {
        const { data } = await client.get(`bolsa/TravelLocation/travellocationitem/${params.id}`);
        getCityHandle(data.data.countryId);
        setCityItemSelection(data.data.cityId);
        setTravelLocItemName(data.data.name);
        setCanSalePart(data.data.canSalePart);
        setIsOther(data.data.isOther);
        setOptionalTypeSelection(data.data.optionalStatus);
        setPaymentTypeSelection(data.data.paymentTypeId);
        setDistrictItemSelection(data.data.districtionId);
        setItemFetched(true);
    };
    useEffect(async () => {
        if (!itemFetched) {
            fetchData();
            dispatch(getTravelLocationItemCurrency(params.id));
        }
    }, [itemFetched]);

    useEffect(async () => {
        if (country.district_fetched) {
            if (districtItemSelection !== '') {
                setDistrictItemSelection(districtItemSelection);
            }
        }
        if (country.city_fetched_finish) {
            if (cityItemSelection !== '') {
                getDistrictHandle(cityItemSelection);
            }
        }
    }, [country]);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleTravelLocationClickOpen = () => {
        setTravelLocationOpen(true);
    };

    const handleTravelLocationClose = () => {
        setTravelLocationOpen(false);
    };

    const handleTravelLocationSave = () => {
        handleTravelLocationClose();
        dispatch(
            addTravelLocationCurrency({
                travelLocationItemId: params.id,
                currencyId: currencySelection,
                price,
                cost
            })
        );
    };

    const handleTravelLocationDeleteClick = (id) => {
        dispatch(deleteTravelLocationItemCurrency(id));
    };

    const handeTravelLocationCellEditCommit = (obj) => {
        if (!Object.prototype.hasOwnProperty.call(obj, 'cellMode')) {
            const comObj = {
                [obj.field]: obj.value
            };
            const newObj = toursettings.travel_location_curr_rows.filter((room_type) => {
                return room_type.id === obj.id;
            });
            const retObj = { ...newObj[0], ...comObj, travelLocationItemId: params.id, currencyId: currencySelection };
            dispatch(updateTravelLocationItemCurrency(retObj));
        }
    };

    const columnsTravelCurrency = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', flex: 1 },
        { field: 'col2', headerName: 'Cost', flex: 1, editable: true },
        { field: 'col3', headerName: 'Price', flex: 1, editable: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleTravelLocationDeleteClick(id)}
                        color="inherit"
                    />
                ];
            }
        }
    ];

    // ==============================|| ROOM TYPE ||============================== //

    return (
        <>
            {!itemFetched ? (
                <MainCard>
                    <Grid container spacing={customizeContants.gridSpacing}>
                        <Box sx={{ width: '100%', height: '400px', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                            <CircularProgress />
                        </Box>
                    </Grid>
                </MainCard>
            ) : (
                <MainCard
                    title="Travel Item Edit"
                    secondary={
                        <Button onClick={handeUpdateTravelLocItem} variant="outlined">
                            Update
                        </Button>
                    }
                >
                    <Grid container spacing={customizeContants.gridSpacing}>
                        <Box sx={{ width: '100%' }}>
                            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                                    <Tab label="General" {...a11yProps(0)} />
                                    <Tab label="Currency" {...a11yProps(1)} />
                                </Tabs>
                            </Box>
                            <TabPanel value={value} index={0}>
                                <Grid item xs={12} sm={12}>
                                    <SubCard title="Address Information">
                                        <Box
                                            sx={{
                                                '& .MuiTextField-root': { m: 1, width: '25ch' }
                                            }}
                                        >
                                            <div>
                                                <TextField
                                                    id="standard-basic"
                                                    label="Name"
                                                    value={locItemName}
                                                    onChange={(ev) => setTravelLocItemName(ev.target.value)}
                                                    variant="standard"
                                                />

                                                <TextField
                                                    id="country"
                                                    select
                                                    label="Country"
                                                    value={countryItemSelection}
                                                    onChange={(ev) => getCityHandle(ev.target.value, true)}
                                                    helperText="Please select Country"
                                                    variant="standard"
                                                >
                                                    {country.country_items.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                                <TextField
                                                    id="city"
                                                    select
                                                    disabled={!country.city_fetched}
                                                    label="City"
                                                    value={cityItemSelection}
                                                    onChange={(ev) => getDistrictHandle(ev.target.value, true)}
                                                    helperText="Please select city"
                                                    variant="standard"
                                                >
                                                    {country.city_by_country_items.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                                <TextField
                                                    id="district"
                                                    select
                                                    disabled={!country.district_fetched}
                                                    label="District"
                                                    value={districtItemSelection}
                                                    onChange={(ev) => setDistrictItemSelection(ev.target.value)}
                                                    helperText="Please select district"
                                                    variant="standard"
                                                >
                                                    {country.district_by_city_items.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                                <TextField
                                                    id="district"
                                                    select
                                                    label="Optional Status"
                                                    value={optinalSelection}
                                                    onChange={(ev) => setOptionalTypeSelection(ev.target.value)}
                                                    helperText="Please select a optinal status"
                                                    variant="standard"
                                                >
                                                    {optionalStatus.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                                <TextField
                                                    id="district"
                                                    select
                                                    label="Payment Type"
                                                    value={paymentTypeSelection}
                                                    onChange={(ev) => setPaymentTypeSelection(ev.target.value)}
                                                    helperText="Please select a payment type"
                                                    variant="standard"
                                                >
                                                    {payment.items.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.name}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                                <FormGroup>
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                defaultChecked={isOther}
                                                                onChange={(ev) => setIsOther(ev.target.checked)}
                                                            />
                                                        }
                                                        label="Is Other"
                                                    />
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                defaultChecked={canSalePart}
                                                                onChange={(ev) => setCanSalePart(ev.target.checked)}
                                                            />
                                                        }
                                                        label="Can sale part"
                                                    />
                                                </FormGroup>
                                            </div>
                                        </Box>
                                    </SubCard>
                                </Grid>
                            </TabPanel>
                            <TabPanel value={value} index={1}>
                                <Grid container spacing={2} alignItems="flexStart">
                                    <Grid item xs={12} md={12}>
                                        <Button variant="outlined" onClick={handleTravelLocationClickOpen}>
                                            Add
                                        </Button>
                                        <Dialog open={roomTypeItemDialog} onClose={handleTravelLocationClose}>
                                            <DialogTitle>Add New Travel Location </DialogTitle>
                                            <DialogContent sx={{ display: 'flex', flexDirection: 'column' }}>
                                                <DialogContentText sx={{ mb: 3 }}>
                                                    To subscribe to this website, please enter your email address here. We will send updates
                                                    occasionally.
                                                </DialogContentText>
                                                <TextField
                                                    autoFocus
                                                    id="currency"
                                                    sx={{ mb: 3 }}
                                                    select
                                                    label="Currency"
                                                    value={currencySelection}
                                                    onChange={(ev) => setCurrencySelection(ev.target.value)}
                                                    helperText="Please select currency"
                                                    variant="standard"
                                                >
                                                    {currency.rows.map((option) => (
                                                        <MenuItem key={option.id} value={option.id}>
                                                            {option.col1}
                                                        </MenuItem>
                                                    ))}
                                                </TextField>
                                                <TextField
                                                    onChange={(e) => setCost(e.target.value)}
                                                    margin="dense"
                                                    id="cost"
                                                    sx={{ mb: 3 }}
                                                    label="Cost"
                                                    value={cost}
                                                    type="number"
                                                    fullWidth
                                                    variant="standard"
                                                />
                                                <TextField
                                                    onChange={(e) => setPrice(e.target.value)}
                                                    margin="dense"
                                                    id="price"
                                                    sx={{ mb: 3 }}
                                                    value={price}
                                                    label="Price"
                                                    type="number"
                                                    fullWidth
                                                    variant="standard"
                                                />
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={handleTravelLocationClose}>Cancel</Button>
                                                <Button onClick={handleTravelLocationSave}>Save</Button>
                                            </DialogActions>
                                        </Dialog>
                                    </Grid>
                                </Grid>
                                <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                                    <DataGrid
                                        rows={toursettings.travel_location_curr_rows || []}
                                        components={{
                                            Toolbar: GridToolbar,
                                            LoadingOverlay: CustomLoadingOverlay
                                        }}
                                        loading={toursettings.travel_location_loading_curr}
                                        pageSize={roomPageSize}
                                        onPageSizeChange={(newPageSize) => setRoomPageSize(newPageSize)}
                                        rowsPerPageOptions={[5, 10, 20]}
                                        onCellEditCommit={handeTravelLocationCellEditCommit}
                                        columns={columnsTravelCurrency}
                                        pagination
                                    />
                                </Grid>
                            </TabPanel>
                        </Box>
                    </Grid>
                </MainCard>
            )}
        </>
    );
};

export default TourEdit;
