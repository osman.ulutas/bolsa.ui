import React, { useState, useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import MainCard from 'ui-component/cards/MainCard';
import { Grid, Link } from '@mui/material';
import { DataGrid, GridToolbar, GridActionsCellItem, GridOverlay } from '@mui/x-data-grid';
import LinearProgress from '@mui/material/LinearProgress';

import Stack from '@mui/material/Stack';
import DeleteIcon from '@mui/icons-material/Delete';
import DetailsIcon from '@mui/icons-material/Details';

// project imports
import Box from '@mui/material/Box';

import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';

import { useNavigate } from 'react-router-dom';

import {
    getRoomType,
    addRoomType,
    deleteRoomType,
    updateRoomType,
    getChildPolicy,
    addChildPolicy,
    deleteChildPolicy,
    updateChildPolicy,
    getCategory,
    addCategory,
    deleteCategory,
    updateCategory,
    getStar,
    addStar,
    deleteStar,
    updateStar,
    getCancellationPolicy,
    addCancellationPolicy,
    deleteCancellationPolicy,
    updateCancellationPolicy
} from 'store/actions/hotelsettings.actions';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div role="tabpanel" hidden={value !== index} id={`simple-tabpanel-${index}`} aria-labelledby={`simple-tab-${index}`} {...other}>
            {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`
    };
}

function CustomLoadingOverlay() {
    return (
        <GridOverlay>
            <div style={{ position: 'absolute', top: 0, width: '100%' }}>
                <LinearProgress />
            </div>
        </GridOverlay>
    );
}

// =============================|| TABLER ICONS ||============================= //

const HotelSettings = () => {
    const history = useNavigate();
    const dispatch = useDispatch();
    const hotelsettings = useSelector((state) => state.hotelsettings);
    const [value, setValue] = React.useState(0);

    // ==============================|| ROOM TYPE ||============================== //

    const [roomTypeDialog, setRoomTypeOpen] = React.useState(false);
    const [name, setName] = useState('');
    const [child, setChild] = useState('');
    const [adult, setAdult] = useState('');
    const [roomPageSize, setRoomPageSize] = useState(5);
    const [selectedRoomTypeRows, setSelectedRoomTypeRows] = React.useState([]);
    const [editRoomTypeRowsModel, setRoomTypeEditRowsModel] = React.useState({});

    const handleRoomTypeClickOpen = () => {
        setRoomTypeOpen(true);
    };

    const handleRoomTypeClose = () => {
        setRoomTypeOpen(false);
    };

    const handleRoomTypeSave = () => {
        dispatch(addRoomType(name, child, adult));
        handleRoomTypeClose();
    };

    const onRoomTypeFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onRoomTypePageChangeHander = (number) => {
        console.log(number);
    };

    const handleRoomTypeDeleteClick = (id) => {
        dispatch(deleteRoomType(id));
    };

    const handeRoomTypeCellEditCommit = (obj) => {
        if (!Object.prototype.hasOwnProperty.call(obj, 'cellMode')) {
            const comObj = {
                [obj.field]: obj.value
            };
            const newObj = hotelsettings.room_type_rows.filter((room_type) => {
                return room_type.id === obj.id;
            });
            const retObj = { ...newObj[0], ...comObj };
            dispatch(updateRoomType(retObj));
        }
    };
    const columnsRoomType = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', flex: 1, editable: true },
        { field: 'col2', headerName: 'Child', flex: 1, editable: true },
        { field: 'col3', headerName: 'Adult', flex: 1, editable: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleRoomTypeDeleteClick(id)}
                        color="inherit"
                    />
                ];
            }
        }
    ];

    // ==============================|| CHILD POLICIY ||============================== //

    const [childPolicyDialog, setChildPolicyOpen] = React.useState(false);
    const [childPolicyName, setChildPolicyName] = useState('');
    const [childPolicyPageSize, setChildPolicyPageSize] = useState(5);
    const [selectedChildPolicyRows, setSelectedChildPolicyRows] = React.useState([]);
    const [editChildPolicyRowsModel, setChildPolicyEditRowsModel] = React.useState({});

    const handleChildPolicyClickOpen = () => {
        setChildPolicyOpen(true);
    };

    const handleChildPolicyClose = () => {
        setChildPolicyOpen(false);
    };

    const handleChildPolicySave = () => {
        dispatch(addChildPolicy(childPolicyName));
        handleChildPolicyClose();
    };

    const onChildPolicyFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onChildPolicyPageChangeHander = (number) => {
        console.log(number);
    };

    const handleChildPolicyDeleteClick = (id) => {
        dispatch(deleteChildPolicy(id));
    };

    const handleChildPolicyDetail = (id) => {
        history({
            pathname: `/hotels/settings/childpolicydetail/${id}`
        });
    };

    const handeChildPolicyCellEditCommit = (obj) => {
        if (!Object.prototype.hasOwnProperty.call(obj, 'cellMode')) {
            const comObj = {
                [obj.field]: obj.value
            };
            const newObj = hotelsettings.child_policy_rows.filter((childPolicy) => {
                return childPolicy.id === obj.id;
            });
            const retObj = { ...newObj[0], ...comObj };
            dispatch(updateChildPolicy(retObj));
        }
    };

    const columnsChildPolicy = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', flex: 1, editable: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DetailsIcon />}
                        label="Detail"
                        onClick={() => handleChildPolicyDetail(id)}
                        color="inherit"
                    />,
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleChildPolicyDeleteClick(id)}
                        color="inherit"
                    />
                ];
            }
        }
    ];

    // ==============================|| CATEGORY POLICIY ||============================== //

    const [categoryDialog, setCategoryOpen] = React.useState(false);
    const [categoryName, setCategoryName] = useState('');
    const [categoryPageSize, setCategoryPageSize] = useState(5);
    const [selectedCategoryRows, setSelectedCategoryRows] = React.useState([]);
    const [editCategoryRowsModel, setCategoryEditRowsModel] = React.useState({});

    const handleCategoryClickOpen = () => {
        setCategoryOpen(true);
    };

    const handleCategoryClose = () => {
        setCategoryOpen(false);
    };

    const handleCategorySave = () => {
        dispatch(addCategory(categoryName));
        handleCategoryClose();
    };

    const onCategoryFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onCategoryPageChangeHander = (number) => {
        console.log(number);
    };

    const handleCategoryDeleteClick = (id) => {
        dispatch(deleteCategory(id));
    };

    const handeCategoryCellEditCommit = (obj) => {
        if (!Object.prototype.hasOwnProperty.call(obj, 'cellMode')) {
            const comObj = {
                [obj.field]: obj.value
            };
            const newObj = hotelsettings.category_policy_rows.filter((category) => {
                return category.id === obj.id;
            });
            const retObj = { ...newObj[0], ...comObj };
            dispatch(updateCategory(retObj));
        }
    };

    const columnsCategory = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', flex: 1, editable: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleCategoryDeleteClick(id)}
                        color="inherit"
                    />
                ];
            }
        }
    ];
    // ==============================|| STAR POLICIY ||============================== //
    const [starDialog, setStarOpen] = React.useState(false);
    const [starName, setStarName] = useState('');
    const [starPageSize, setStarPageSize] = useState(5);
    const [selectedStarRows, setSelectedStarRows] = React.useState([]);
    const [editStarRowsModel, setStarEditRowsModel] = React.useState({});

    const handleStarClickOpen = () => {
        setStarOpen(true);
    };

    const handleStarClose = () => {
        setStarOpen(false);
    };

    const handleStarSave = () => {
        dispatch(addStar(starName));
        handleStarClose();
    };

    const onStarFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onStarPageChangeHander = (number) => {
        console.log(number);
    };

    const handleStarDeleteClick = (id) => {
        dispatch(deleteStar(id));
    };

    const handeStarCellEditCommit = (obj) => {
        if (!Object.prototype.hasOwnProperty.call(obj, 'cellMode')) {
            const comObj = {
                [obj.field]: obj.value
            };
            const newObj = hotelsettings.star_rows.filter((star) => {
                return star.id === obj.id;
            });
            const retObj = { ...newObj[0], ...comObj };
            dispatch(updateStar(retObj));
        }
    };

    const columnsStar = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Stars', flex: 1, editable: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem icon={<DeleteIcon />} label="Delete" onClick={() => handleStarDeleteClick(id)} color="inherit" />
                ];
            }
        }
    ];

    // ==============================|| CANCEELLATION POLICIY ||============================== //

    const [cancellationDialog, setCancellationOpen] = React.useState(false);
    const [cancellationName, setCancellationName] = useState('');
    const [cancellationPageSize, setCancellationPageSize] = useState(5);
    const [selectedCancellationRows, setSelectedCancellationRows] = React.useState([]);
    const [editCancellationRowsModel, setCancellationEditRowsModel] = React.useState({});

    const handleCancellationClickOpen = () => {
        setCancellationOpen(true);
    };

    const handleCancellationClose = () => {
        setCancellationOpen(false);
    };

    const handleCancellationSave = () => {
        dispatch(addCancellationPolicy(cancellationName));
        handleCancellationClose();
    };

    const onCancellationFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onCancellationPageChangeHander = (number) => {
        console.log(number);
    };

    const handleCancellationDeleteClick = (id) => {
        dispatch(deleteCancellationPolicy(id));
    };

    const handleCancellationDetail = (id) => {
        history({
            pathname: `/hotels/settings/cancellationpolicydetail/${id}`
        });
    };

    const handeCancellationCellEditCommit = (obj) => {
        if (!Object.prototype.hasOwnProperty.call(obj, 'cellMode')) {
            const comObj = {
                [obj.field]: obj.value
            };
            const newObj = hotelsettings.cancellation_policy_rows.filter((cancellation) => {
                return cancellation.id === obj.id;
            });
            const retObj = { ...newObj[0], ...comObj };
            dispatch(updateCancellationPolicy(retObj));
        }
    };

    const columnsCancellation = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', flex: 1, editable: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DetailsIcon />}
                        label="Detail"
                        onClick={() => handleCancellationDetail(id)}
                        color="inherit"
                    />,
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleCancellationDeleteClick(id)}
                        color="inherit"
                    />
                ];
            }
        }
    ];

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    useEffect(() => {}, []);

    return (
        <MainCard title="Hotel Settings">
            <Box sx={{ width: '100%' }}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                        <Tab label="Room Type" {...a11yProps(0)} />
                        <Tab label="Child Policy" {...a11yProps(1)} />
                        <Tab label="Category" {...a11yProps(2)} />
                        <Tab label="Star" {...a11yProps(3)} />
                        <Tab label="Cancellation Policy" {...a11yProps(4)} />
                    </Tabs>
                </Box>
                <TabPanel value={value} index={0}>
                    <Grid container spacing={2} alignItems="flexStart">
                        <Grid item xs={12} md={12}>
                            <Button variant="outlined" onClick={handleRoomTypeClickOpen}>
                                New
                            </Button>
                            <Dialog open={roomTypeDialog} onClose={handleRoomTypeClose}>
                                <DialogTitle>Add New Room Type</DialogTitle>
                                <DialogContent>
                                    <DialogContentText>
                                        To subscribe to this website, please enter your email address here. We will send updates
                                        occasionally.
                                    </DialogContentText>
                                    <TextField
                                        autoFocus
                                        onChange={(e) => setName(e.target.value)}
                                        margin="dense"
                                        id="name"
                                        label="Name"
                                        type="text"
                                        fullWidth
                                        variant="standard"
                                    />
                                    <TextField
                                        margin="dense"
                                        onChange={(e) => setChild(e.target.value)}
                                        id="child"
                                        label="Child"
                                        type="number"
                                        fullWidth
                                        variant="standard"
                                    />

                                    <TextField
                                        margin="dense"
                                        onChange={(e) => setAdult(e.target.value)}
                                        id="adult"
                                        label="Adult"
                                        type="number"
                                        fullWidth
                                        variant="standard"
                                    />
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleRoomTypeClose}>Cancel</Button>
                                    <Button onClick={handleRoomTypeSave}>Save</Button>
                                </DialogActions>
                            </Dialog>
                        </Grid>
                        <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                            <DataGrid
                                rows={hotelsettings.room_type_rows || []}
                                components={{
                                    Toolbar: GridToolbar,
                                    LoadingOverlay: CustomLoadingOverlay
                                }}
                                loading={hotelsettings.room_type_loading}
                                pageSize={roomPageSize}
                                onPageSizeChange={(newPageSize) => setRoomPageSize(newPageSize)}
                                rowsPerPageOptions={[5, 10, 20]}
                                onFilterModelChange={onRoomTypeFilterModelChangeHandler}
                                onPageChange={onRoomTypePageChangeHander}
                                onCellEditCommit={handeRoomTypeCellEditCommit}
                                columns={columnsRoomType}
                                pagination
                            />
                        </Grid>
                    </Grid>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <Grid container spacing={2} alignItems="flexStart">
                        <Grid item xs={12} md={12}>
                            <Button variant="outlined" onClick={handleChildPolicyClickOpen}>
                                New
                            </Button>
                            <Dialog open={childPolicyDialog} onClose={handleChildPolicyClose}>
                                <DialogTitle>Add New Child Policy</DialogTitle>
                                <DialogContent>
                                    <DialogContentText>
                                        To subscribe to this website, please enter your email address here. We will send updates
                                        occasionally.
                                    </DialogContentText>
                                    <TextField
                                        autoFocus
                                        onChange={(e) => setChildPolicyName(e.target.value)}
                                        margin="dense"
                                        id="childPolicyName"
                                        label="Name"
                                        type="text"
                                        fullWidth
                                        variant="standard"
                                    />
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleChildPolicyClose}>Cancel</Button>
                                    <Button onClick={handleChildPolicySave}>Save</Button>
                                </DialogActions>
                            </Dialog>
                        </Grid>
                        <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                            <DataGrid
                                rows={hotelsettings.child_policy_rows || []}
                                components={{
                                    Toolbar: GridToolbar,
                                    LoadingOverlay: CustomLoadingOverlay
                                }}
                                loading={hotelsettings.chlid_policy_loading}
                                pageSize={childPolicyPageSize}
                                onPageSizeChange={(newPageSize) => setChildPolicyPageSize(newPageSize)}
                                rowsPerPageOptions={[5, 10, 20]}
                                onFilterModelChange={onChildPolicyFilterModelChangeHandler}
                                onPageChange={onChildPolicyPageChangeHander}
                                onCellEditCommit={handeChildPolicyCellEditCommit}
                                columns={columnsChildPolicy}
                                pagination
                            />
                        </Grid>
                    </Grid>
                </TabPanel>
                <TabPanel value={value} index={2}>
                    <Grid container spacing={2} alignItems="flexStart">
                        <Grid item xs={12} md={12}>
                            <Button variant="outlined" onClick={handleCategoryClickOpen}>
                                New
                            </Button>
                            <Dialog open={categoryDialog} onClose={handleCategoryClose}>
                                <DialogTitle>Add New Category Policy</DialogTitle>
                                <DialogContent>
                                    <DialogContentText>
                                        To subscribe to this website, please enter your email address here. We will send updates
                                        occasionally.
                                    </DialogContentText>
                                    <TextField
                                        autoFocus
                                        onChange={(e) => setCategoryName(e.target.value)}
                                        margin="dense"
                                        id="categoryName"
                                        label="Name"
                                        type="text"
                                        fullWidth
                                        variant="standard"
                                    />
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleCategoryClose}>Cancel</Button>
                                    <Button onClick={handleCategorySave}>Save</Button>
                                </DialogActions>
                            </Dialog>
                        </Grid>
                        <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                            <DataGrid
                                rows={hotelsettings.category_policy_rows || []}
                                components={{
                                    Toolbar: GridToolbar,
                                    LoadingOverlay: CustomLoadingOverlay
                                }}
                                loading={hotelsettings.category_policy_loading}
                                pageSize={categoryPageSize}
                                onPageSizeChange={(newPageSize) => setCategoryPageSize(newPageSize)}
                                rowsPerPageOptions={[5, 10, 20]}
                                onFilterModelChange={onCategoryFilterModelChangeHandler}
                                onPageChange={onCategoryPageChangeHander}
                                onCellEditCommit={handeCategoryCellEditCommit}
                                columns={columnsCategory}
                                pagination
                            />
                        </Grid>
                    </Grid>
                </TabPanel>
                <TabPanel value={value} index={3}>
                    <Grid container spacing={2} alignItems="flexStart">
                        <Grid item xs={12} md={12}>
                            <Button variant="outlined" onClick={handleStarClickOpen}>
                                New
                            </Button>
                            <Dialog open={starDialog} onClose={handleStarClose}>
                                <DialogTitle>Add New Star Policy</DialogTitle>
                                <DialogContent>
                                    <DialogContentText>
                                        To subscribe to this website, please enter your email address here. We will send updates
                                        occasionally.
                                    </DialogContentText>
                                    <TextField
                                        autoFocus
                                        onChange={(e) => setStarName(e.target.value)}
                                        margin="dense"
                                        id="stars"
                                        label="Name"
                                        type="text"
                                        fullWidth
                                        variant="standard"
                                    />
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleStarClose}>Cancel</Button>
                                    <Button onClick={handleStarSave}>Save</Button>
                                </DialogActions>
                            </Dialog>
                        </Grid>
                        <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                            <DataGrid
                                rows={hotelsettings.star_rows || []}
                                components={{
                                    Toolbar: GridToolbar,
                                    LoadingOverlay: CustomLoadingOverlay
                                }}
                                loading={hotelsettings.star_loading}
                                pageSize={starPageSize}
                                onPageSizeChange={(newPageSize) => setStarPageSize(newPageSize)}
                                rowsPerPageOptions={[5, 10, 20]}
                                onFilterModelChange={onStarFilterModelChangeHandler}
                                onPageChange={onStarPageChangeHander}
                                onCellEditCommit={handeStarCellEditCommit}
                                columns={columnsStar}
                                pagination
                            />
                        </Grid>
                    </Grid>
                </TabPanel>
                <TabPanel value={value} index={4}>
                    <Grid container spacing={2} alignItems="flexStart">
                        <Grid item xs={12} md={12}>
                            <Button variant="outlined" onClick={handleCancellationClickOpen}>
                                New
                            </Button>
                            <Dialog open={cancellationDialog} onClose={handleCancellationClose}>
                                <DialogTitle>Add New Cancellation Policy</DialogTitle>
                                <DialogContent>
                                    <DialogContentText>
                                        To subscribe to this website, please enter your email address here. We will send updates
                                        occasionally.
                                    </DialogContentText>
                                    <TextField
                                        autoFocus
                                        onChange={(e) => setCancellationName(e.target.value)}
                                        margin="dense"
                                        id="cancellationName"
                                        label="Name"
                                        type="text"
                                        fullWidth
                                        variant="standard"
                                    />
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleCancellationClose}>Cancel</Button>
                                    <Button onClick={handleCancellationSave}>Save</Button>
                                </DialogActions>
                            </Dialog>
                        </Grid>
                        <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                            <DataGrid
                                rows={hotelsettings.cancellation_policy_rows || []}
                                components={{
                                    Toolbar: GridToolbar,
                                    LoadingOverlay: CustomLoadingOverlay
                                }}
                                loading={hotelsettings.cancellation_policy_loading}
                                pageSize={cancellationPageSize}
                                onPageSizeChange={(newPageSize) => setCancellationPageSize(newPageSize)}
                                rowsPerPageOptions={[5, 10, 20]}
                                onFilterModelChange={onCancellationFilterModelChangeHandler}
                                onPageChange={onCancellationPageChangeHander}
                                onCellEditCommit={handeCancellationCellEditCommit}
                                columns={columnsCancellation}
                                pagination
                            />
                        </Grid>
                    </Grid>
                </TabPanel>
            </Box>
        </MainCard>
    );
};

export default HotelSettings;
