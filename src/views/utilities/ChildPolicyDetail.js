import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import Box from '@mui/material/Box';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import MainCard from 'ui-component/cards/MainCard';
import { Grid } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { useParams } from 'react-router-dom';
import { getChildPolicyItem, addChildPolicyItem, deleteChildPolicyItem, updateChildPolicyItem } from 'store/actions/hotelsettings.actions';

import { DataGrid, GridToolbar, GridActionsCellItem, GridOverlay } from '@mui/x-data-grid';
import LinearProgress from '@mui/material/LinearProgress';

// ==============================|| CHILD POLICIY ||============================== //

function CustomLoadingOverlay() {
    return (
        <GridOverlay>
            <div style={{ position: 'absolute', top: 0, width: '100%' }}>
                <LinearProgress />
            </div>
        </GridOverlay>
    );
}

const ChildPolicyDetail = () => {
    const params = useParams();
    const dispatch = useDispatch();
    const hotelsettings = useSelector((state) => state.hotelsettings);

    console.log(params.id);

    const [chilPolicyDialog, setChildPolicyOpen] = React.useState(false);
    const [childName, setChildName] = useState('');
    const [minAge, setMinAge] = useState('');
    const [maxAge, setMaxAge] = useState('');
    const [discount, setdiscount] = useState('');
    const [childPageSize, setChildPolicyPageSize] = useState(5);
    const [selectedChildPolicyRows, setSelectedChildPolicyRows] = React.useState([]);
    const [editChildPolicyRowsModel, setChildPolicyEditRowsModel] = React.useState({});

    const handleChildPolicyClickOpen = () => {
        setChildPolicyOpen(true);
    };

    const handleChildPolicyClose = () => {
        setChildPolicyOpen(false);
    };

    const handleChildPolicySave = () => {
        dispatch(addChildPolicyItem(childName, minAge, maxAge, discount, params.id));
        handleChildPolicyClose();
    };

    const onChildPolicyFilterModelChangeHandler = (filter) => {
        console.log(filter);
    };
    const onChildPolicyPageChangeHander = (number) => {
        console.log(number);
    };

    const handleChildPolicyDeleteClick = (id) => {
        dispatch(deleteChildPolicyItem(id));
    };

    const handeChildPolicyCellEditCommit = (obj) => {
        if (!Object.prototype.hasOwnProperty.call(obj, 'cellMode')) {
            const comObj = {
                [obj.field]: obj.value
            };
            const newObj = hotelsettings.child_policy_item_rows.filter((child_policy) => {
                return child_policy.id === obj.id;
            });
            const retObj = { ...newObj[0], ...comObj };
            dispatch(updateChildPolicyItem(retObj));
        }
    };

    const columnsChildPolicy = [
        { field: 'id', hide: true },
        { field: 'col1', headerName: 'Name', flex: 1, editable: true, hide: true },
        { field: 'col2', headerName: 'Min Age', flex: 1, editable: true },
        { field: 'col3', headerName: 'Max Age', flex: 1, editable: true },
        { field: 'col4', headerName: 'Discount', flex: 1, editable: true },
        {
            field: 'actions',
            headerName: 'Actions',
            type: 'actions',
            width: 80,
            getActions: ({ id }) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={() => handleChildPolicyDeleteClick(id)}
                        color="inherit"
                    />
                ];
            }
        }
    ];

    useEffect(() => {
        dispatch(getChildPolicyItem(params.id));
    }, []);

    return (
        <MainCard title="Child Policy Detail">
            <Box sx={{ width: '100%' }}>
                <Grid container spacing={2} alignItems="flexStart">
                    <Grid item xs={12} md={12}>
                        <Button variant="outlined" onClick={handleChildPolicyClickOpen}>
                            New
                        </Button>
                        <Dialog open={chilPolicyDialog} onClose={handleChildPolicyClose}>
                            <DialogTitle>Add New Child Policy Item</DialogTitle>
                            <DialogContent>
                                <DialogContentText>
                                    To subscribe to this website, please enter your email address here. We will send updates occasionally.
                                </DialogContentText>
                                <TextField
                                    margin="dense"
                                    onChange={(e) => setMinAge(e.target.value)}
                                    id="minAge"
                                    label="Min Age"
                                    type="number"
                                    fullWidth
                                    variant="standard"
                                />

                                <TextField
                                    margin="dense"
                                    onChange={(e) => setMaxAge(e.target.value)}
                                    id="maxAge"
                                    label="Max Age"
                                    type="number"
                                    fullWidth
                                    variant="standard"
                                />
                                <TextField
                                    margin="dense"
                                    onChange={(e) => setdiscount(e.target.value)}
                                    id="discount"
                                    label="discount"
                                    type="number"
                                    fullWidth
                                    variant="standard"
                                />
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleChildPolicyClose}>Cancel</Button>
                                <Button onClick={handleChildPolicySave}>Save</Button>
                            </DialogActions>
                        </Dialog>
                    </Grid>
                    <Grid item xs={12} md={12} style={{ height: 500 }} sx={{ pt: 2, pb: 5 }}>
                        <DataGrid
                            rows={hotelsettings.child_policy_item_rows || []}
                            components={{
                                Toolbar: GridToolbar,
                                LoadingOverlay: CustomLoadingOverlay
                            }}
                            loading={hotelsettings.child_policy_item_loading}
                            pageSize={childPageSize}
                            onPageSizeChange={(newPageSize) => setChildPolicyPageSize(newPageSize)}
                            rowsPerPageOptions={[5, 10, 20]}
                            onFilterModelChange={onChildPolicyFilterModelChangeHandler}
                            onPageChange={onChildPolicyPageChangeHander}
                            onCellEditCommit={handeChildPolicyCellEditCommit}
                            columns={columnsChildPolicy}
                            pagination
                        />
                    </Grid>
                </Grid>
            </Box>
        </MainCard>
    );
};

export default ChildPolicyDetail;
